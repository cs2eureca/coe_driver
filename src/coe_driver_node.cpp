
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>
#include <driver_base/driver.h>
#include <driver_base/driver_node.h>


#include <itia_futils/itia_futils.h>
#include <itia_futils/string.h>
#include <itia_rtutils/itia_rtutils.h>
#include <itia_butils/itia_butils.h>

#include <coe_core/coe_sdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_soem_utilities/coe_soem_utilities.h>

#include <coe_driver/CoeDriverConfig.h>

#include <coe_driver/modules/coe_module_descriptor.h>
#include <coe_driver/modules/coe_network_descriptor.h>
#include <coe_driver/modules/coe_pdo_shared_memory.h>
#include <coe_driver/modules/coe_srv_utilities.h>

#include "coe_driver_utilities.h"
#include "coe_driver.h"

typedef std::map< ec_err_type,  std::shared_ptr< diagnostic_updater::FunctionDiagnosticTask > > TypedFunctionDiagnosticTask;
typedef std::map< int, TypedFunctionDiagnosticTask > NodesTypedFunctionDiagnosticTask;


class CoeDriverNode : public driver_base::DriverNode< CoeDriver >
{
private:
  std::string connect_fail_;
  
  double desired_freq_;

  ros::NodeHandle                       node_handle_;
  std::shared_ptr<SdoManager>           sdo_;
  
  std::shared_ptr<ros::ServiceServer>   reset_error_service_;
  bool resetErrors( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
  {
    return driver_.resetErrors();
  }
  


public:
  
  CoeDriverNode(ros::NodeHandle& nh)
  : driver_base::DriverNode< CoeDriver >(nh)
  , node_handle_(nh)
  {
    if( node_handle_.hasParam("verbose") )
    {
      ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
    }
    
    desired_freq_ = 0;
    
    driver_.setPostOpenHook(boost::bind(&CoeDriverNode::postOpenHook, this));
   
    diagnostic_.add("RT Communication", boost::bind(&CoeDriverNode::nodeDiagnostics     , this, _1));
    diagnostic_.add("SDO Queries"     , boost::bind(&CoeDriverNode::sdoQueryDiagnostics , this, _1));
    
    reset_error_service_.reset( new ros::ServiceServer( nh.advertiseService ( "reset_errors", &CoeDriverNode::resetErrors, this) ) );
  }
  
  void postOpenHook()
  {
    diagnostic_.setHardwareID(driver_.getID());
    
    const std::map<int,std::string>& add_map = driver_.getAddressUniqueIdMap();
    std::map< int, coe_driver::ModuleDiagnosticPtr >& driver_sorted_typed_errors = driver_.getCoeDriverTypedErrors();
    
    for( auto it=driver_sorted_typed_errors.begin();it!=driver_sorted_typed_errors.end();it++ )
    {
      std::string                       node_name = add_map.at( it->first );
      coe_driver::ModuleDiagnosticPtr&  node_typed_errors = it->second;
      
      diagnostic_.add ( node_name 
                      , boost::bind(&coe_driver::ModuleDiagnostic::coeDiagnostics, node_typed_errors.get(), _1)  );
      
    }    
    diagnostic_.add ( "Generic Errors", boost::bind(&coe_driver::MasterDiagnostic::coeDiagnostics, driver_.getCoeDriverGenericErrors().get(), _1)  );
    
    sdo_.reset( new SdoManager( driver_.getNetworkDescriptor(), true ) );
  }
  
  void addOpenedTests()           { ROS_WARN("Not yet any test managed"); }
  void addStoppedTests()          { ROS_WARN("Not yet any test managed"); }
  void addRunningTests()          { ROS_WARN("Not yet any test managed"); }
  void addDiagnostics()           { }
  void reconfigureHook(int level) { diagnostic_.force_update();     }
  
  
  void nodeDiagnostics(diagnostic_updater::DiagnosticStatusWrapper& status)
  {
    std::string t = " [" + boost::posix_time::to_simple_string( ros::Time::now().toBoost() ) + "] ";
    if (driver_.getState() == driver_.CLOSED)       status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Not connected. " + connect_fail_ + t);
    else if (driver_.getState() == driver_.RUNNING) status.summary(diagnostic_msgs::DiagnosticStatus::OK, "Streaming" + t);
    else if (driver_.getState() == driver_.OPENED)  status.summary(diagnostic_msgs::DiagnosticStatus::OK, "Open");
    else                                            status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Unknown state"+ t);

    if( driver_.getData() == NULL )
    {
      ROS_WARN("Data not yet in the queue");
      return;
    }
    status.add("Window Time           [s]", driver_.getData()->getWindowDim() * driver_.getData()->getCycleTime() );
    status.add("[MASTER] Mean Missed Messages  [-]", driver_.getData()->getMeanMissedCycles() );
    status.add("[MASTER] Max Missed Messages   [-]", driver_.getData()->getMaxMissedCycles() );
    status.add("[MASTER] Mean Calc Time       [ms]", driver_.getData()->getMeanCalcTime() );
    status.add("[MASTER] Max Calc Time        [ms]", driver_.getData()->getMaxCalcTime() );
    status.add("[MASTER] Mean Wkc              [-]", driver_.getData()->getMeanWkc() );
    status.add("[MASTER] Max Wkc               [-]", driver_.getData()->getMaxWkc() );
    status.add("[CLIENT] Mean latency         [ms]", driver_.getData()->getMeanLatencyTime() );
    status.add("[CLIENT] Max latency          [ms]", driver_.getData()->getMaxLatencyTime() );
    auto rxbond = driver_.getData()->getRxBonded();
    for( auto const & b : rxbond )
      status.add(std::string( "RxBonded [" + b.first + "]" ).c_str(), b.second );
    
    auto txbond = driver_.getData()->getTxBonded();
    for( auto const & b : txbond )
      status.add(std::string( "TxBonded [" + b.first + "]" ).c_str(), b.second );

    auto rxhardrt = driver_.getData()->getRxHardRT();
    for( auto const & b : rxhardrt )
      status.add(std::string( "RxHardRT [" + b.first + "]" ).c_str(), b.second );
    
    auto txhardrt = driver_.getData()->getRxHardRT();
    for( auto const & b : txhardrt )
      status.add(std::string( "TxHardRT[" + b.first + "]" ).c_str(), b.second );

  }
  
  void sdoQueryDiagnostics(diagnostic_updater::DiagnosticStatusWrapper& status)
  {
    std::string t  = " [" + boost::posix_time::to_simple_string( ros::Time::now().toBoost() ) + "] ";
    
    int cnt_notok = 0; 
    if( sdo_ )
    {
      for( auto sdo_query : sdo_->getQueries( ) )
      {
        status.add( std::get<0>(sdo_query), std::get<1>(sdo_query));
        cnt_notok = ( std::get<2>(sdo_query) ? cnt_notok : cnt_notok++ );
      }
    }
    
    if( cnt_notok == 0 ) 
      status.summary(diagnostic_msgs::DiagnosticStatus::OK, "None Issue " + t);
    else
      status.summary(diagnostic_msgs::DiagnosticStatus::WARN, std::to_string( cnt_notok ) + "Issues " + t);
  }
  
    
 
  
};

int main(int argc, char* argv[])
{
  if( !itia_rtutils::rt_main_init(PRE_ALLOCATION_SIZE) )
  {
    perror("Error in rt_main_init. Exit. Have you launched the node as superuser?");
    return -1;
  }
  
  return driver_base::main<CoeDriverNode>(argc, argv, "coe_driver");
  
}











