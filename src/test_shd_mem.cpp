#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <cstring>
#include <cstdlib>
#include <string>
#include <limits>

#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <itia_rtutils/itia_rtutils.h>
#include <coe_driver/coe_module_descriptor.h>
#include <coe_driver/coe_network_descriptor.h>
#include <coe_driver/coe_shared_memory.h>

static const size_t         PRE_ALLOCATION_SIZE             = 100*1024*1024;
static const size_t         MY_STACK_SIZE                   = 100*1024;
static const std::string    COE_DEVICE_PARAMETER_NAMESPACE  = "/coe_driver_node/coe";


class CoeShmAccessor
{
  
protected:
  
  ros::NodeHandle                     nh_;
  coe_driver::NetworkDescriptor   network_;
  coe_driver::ModulesSharedMemory pdo_shared_memory_;


public:
  
  
  CoeShmAccessor(ros::NodeHandle& nh, const std::string& coe_device_parameter_namespace)
  : nh_( nh )
  , network_(nh,coe_device_parameter_namespace)
  {

    if(!init())
    {
      ROS_ERROR("Failed in ctor. Exit.");
      throw std::runtime_error("Failed in ctor. Exit.");
    }
  }
  
  
  bool init()
  {

    ROS_INFO("Extract the coe configuration information from ros param server");
    network_.initNames();
    
    network_.initNodes( );
    ROS_INFO("Prepare the shared memory...");
    for( auto const & module : network_.getModules() )
    {
      coe_driver::ModuleSharedMemoryPtr shm_mem( new coe_driver::ModuleSharedMemory( module, coe_driver::PdoSharedMemory::OPEN ) );
      pdo_shared_memory_.insert( shm_mem );
    }

    return true;
  }
  
  void run( ) 
  {
    ros::WallRate loop_rate(10);
    
    std::map< std::string , uint8_t* > tx_pdo_map;
    std::map< std::string , uint8_t* > rx_pdo_map;
    for( auto & module: network_.getModules() )
    {
      tx_pdo_map[module->getIdentifier()] = new uint8_t[ module->getTxPdo().nbytes(true) ];
      rx_pdo_map[module->getIdentifier()] = new uint8_t[ module->getRxPdo().nbytes(true) ];
      
      std::memset( tx_pdo_map[module->getIdentifier()], 0x0, module->getTxPdo().nbytes(true) );
      std::memset( rx_pdo_map[module->getIdentifier()], 0x0, module->getRxPdo().nbytes(true) );
      
    }
    
    while (ros::ok() )
    {
      
      for( auto & module: network_.getModules() )
      {
        pdo_shared_memory_[ module->getIdentifier() ]->tx_pdo_ >> tx_pdo_map[ module->getIdentifier() ];
        pdo_shared_memory_[ module->getIdentifier() ]->rx_pdo_ << rx_pdo_map[ module->getIdentifier() ];
        
        module->updateInputs  ( tx_pdo_map[ module->getIdentifier() ], false );
        module->updateOutputs ( rx_pdo_map[ module->getIdentifier() ], false );

        std::cout << module->to_string("both");
      }
      loop_rate.sleep();
    }
  }
};

int main(int argc, char *argv[])
{
  
  if( !itia_rtutils::rt_main_init(PRE_ALLOCATION_SIZE) )
  {
    perror("Error in rt_main_init. Exit.");
    return -1;
  }

  ros::init(argc,argv,"test_shd_mem");
  ros::NodeHandle nh("~");

  try
  {

    CoeShmAccessor coe_shm_accessor( nh, COE_DEVICE_PARAMETER_NAMESPACE);
    
    coe_shm_accessor.run();

  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s", e.what());
    ROS_ERROR("Abort.");
    return -1;
  }
  catch (...)
  {
    ROS_ERROR("Unhandled exception ");
    ROS_ERROR("Abort.");
    return -1;
  }
  
  return 0;
}