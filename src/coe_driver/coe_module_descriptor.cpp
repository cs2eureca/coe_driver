/**
 *
 * @file coe_utilities.h
 * @brief FIle with some utility for the management of the Can Over Ethercat protocol
 *
 */
#include <regex>
#include <boost/interprocess/shared_memory_object.hpp>
#include <XmlRpc.h>
#include <itia_futils/itia_futils.h>
#include <itia_rutils/itia_rutils.h>
#include <itia_rutils/itia_rutils_xmlrpc.h>
#include <coe_core/coe_base.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_sdo.h>
#include <coe_core/coe_sdo_xmlrpc.h>
#include <coe_core/coe_pdo_xmlrpc.h>
#include <coe_soem_utilities/coe_soem_utilities.h>
#include <coe_driver/modules/coe_module_descriptor.h>
#include <coe_driver/modules/coe_module_descriptor_xmlrpc.h>

#define HDR \
  std::string ( std::string(RESET) + std::string("[-----]") \
              + "[ " \
              + ( BOLDCYAN + std::to_string(address_) +"# " + unique_id_ + RESET ) \
              + ( BOLDCYAN + std::string( default_config_ ? " - DEFAULT CFG" : " - PARAMS CFG" ) + RESET )\
              + " ]" )

namespace coe_driver
{
  
bool ModuleDescriptor::initHandles(  ) 
{
  try
  {
    
    XmlRpc::XmlRpcValue root_config;      
    itia::rutils::extractParam( nh_, root_param_, root_config );
  
    XmlRpc::XmlRpcValue root( root_config );
    if( !root.hasMember( param_label_.c_str() ) )
    {
      ROS_ERROR("The module '%s' has not parameters in the ros parame server.", param_label_.c_str() );
      return false;
    }
    
    XmlRpc::XmlRpcValue& node = root[param_label_.c_str()];
  
    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::AXIS_FEEDBACK ] )  )
    {
       coe_driver::XmlRpcAxisData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::AXIS_FEEDBACK] ], axis_feedback_, "axis feedback" );
    }
    else ROS_DEBUG("[CHECK] None Axis Feedback Handle in rosparam server");
    
    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::AXIS_COMMAND ] )  )
    {
       coe_driver::XmlRpcAxisData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::AXIS_COMMAND] ], axis_command_, "axis feedback" );
    }
    else ROS_DEBUG("[CHECK] None Axis Feedback Handle in rosparam server");

    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::ANALOG_INPUTS] )  )
    {
       coe_driver::XmlRpcAnalogData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::ANALOG_INPUTS] ], analog_inputs_, "analog inputs" );
    }
    else ROS_DEBUG("[CHECK] None Analog Input Handle in rosparam server");
    
    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::ANALOG_OUTPUTS] )  )
    {
       coe_driver::XmlRpcAnalogData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::ANALOG_OUTPUTS] ], analog_outputs_, "analog outputs" );
    }
    else ROS_DEBUG("[CHECK] None Analog Output Handle in rosparam server");
    
    
    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::DIGITAL_INPUTS] )  )
    {
       coe_driver::XmlRpcDigitalData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::DIGITAL_INPUTS] ], digital_inputs_, "digital inputs" );
    }
    else ROS_DEBUG("[CHECK] None Digital Input Handle in rosparam server");
    
    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::DIGITAL_OUTPUTS] )  )
    {
       coe_driver::XmlRpcDigitalData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::DIGITAL_OUTPUTS] ], digital_outputs_, "digital outputs" );
    }
    else ROS_DEBUG("[CHECK] None Digital Output Handle in rosparam server");

    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::WORD_INPUTS] )  )
    {
       coe_driver::XmlRpcWordData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::WORD_INPUTS] ], word_inputs_, "word inputs" );
    }
    else ROS_DEBUG("[CHECK] None Digital Input Handle in rosparam server");
    
    if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::WORD_OUTPUTS] )  )
    {
       coe_driver::XmlRpcWordData::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::WORD_OUTPUTS] ], word_outputs_, "word outputs" );
    }
    else ROS_DEBUG("[CHECK] None Digital Output Handle in rosparam server");
    
  }
  catch( XmlRpc::XmlRpcException& e )
  {
    ROS_ERROR("%s XmlRpcException: %s", HDR.c_str(), e.getMessage().c_str() );
    return false;
  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s Exception: %s", HDR.c_str(), e.what() );
    return false;
  }
  
  return true;
}

bool ModuleDescriptor::initNodeConfigurationFromParams(  ) 
{
  if( configurated_names_ )
  {
    ROS_ERROR("The module labelled with '%s' has already the names configured. Check.", unique_id_.c_str() );
    return false;
  }
  
  XmlRpc::XmlRpcValue root_config;      
  itia::rutils::extractParam( nh_, root_param_, root_config );

  try
  {
    
    XmlRpc::XmlRpcValue root( root_config );
    if( !root.hasMember( param_label_.c_str() ) )
    {
      ROS_ERROR("The module labelled with '%s/%s/%s' has not parameters in the ros parame server.", nh_.getNamespace().c_str(), root_param_.c_str(), param_label_.c_str() );
      return false;
    }
  }
  catch( XmlRpc::XmlRpcException& e )
  {
    ROS_ERROR("%s XmlRpcException: %s", HDR.c_str(), e.getMessage().c_str() );
    return false;
  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s Exception: %s", HDR.c_str(),  e.what() );
    return false;
  }
  
  description_          = "TODO";
  model_                = "TODO";
  model_                = "TODO";
  loop_rate_decimation_ = 1;
  watchdog_decimation_  = 1;
  try
  {
    XmlRpc::XmlRpcValue root( root_config );
    XmlRpc::XmlRpcValue& node = root[param_label_.c_str()];

    description_          = itia::rutils::toString( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::DESCRIPTION           ] , "description"          ); // exception if error
    model_                = itia::rutils::toString( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::MODEL                 ] , "model"                ); // exception if error
    loop_rate_decimation_ = itia::rutils::toInt   ( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::LOOP_RATE_DECIMATION  ] , "loop rate decimation" ); // exception if error
    watchdog_decimation_  = itia::rutils::toDouble( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::WATCHDOG_DECIMATION   ] , "Wathcdog"             ); // exception if error
  }
  catch( std::exception& e )
  {
    ROS_WARN("%s %s", HDR.c_str(),  e.what() );
  }
  
  return true;

}

bool ModuleDescriptor::initNodeCoeConfigurationFromParams( bool configure_sdo ) 
{
  if( configurated_names_ )
  {
    ROS_ERROR("The module labelled with '%s' has already the names configured. Check.", unique_id_.c_str() );
    return false;
  }
  
  XmlRpc::XmlRpcValue root_config;      
  itia::rutils::extractParam( nh_, root_param_, root_config );
  
  try
  {
    
    XmlRpc::XmlRpcValue root( root_config );
    if( !root.hasMember( param_label_.c_str() ) )
    {
      ROS_ERROR("The module labelled with '%s' has not parameters in the ros parame server.", param_label_.c_str() );
      return false;
    }
    
    XmlRpc::XmlRpcValue& node = root[param_label_.c_str()];
      
    enable_dc_            = itia::rutils::toBool  ( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::ENABLE_DC             ] , "enable dc"            ); // exception if error
    support_sdoca_        = itia::rutils::toBool  ( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::SUPPORT_SDO_CA        ] , "support sdoca"        ); // exception if error
    
    if( !default_config_ )
    {
      if( configure_sdo  )
      {
        if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::SDO ] )  )
        {
          coe_core::XmlRpcSdo::fromXmlRpcValue(node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::SDO ] ], configuration_sdo_, "sdo" );
        }
      }
      
      if(  node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::RXPDO ] )  )
      {
        coe_core::XmlRpcPdo::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::RXPDO ] ], rx_pdo_, "rxpdo" );
      }
      
      if(  node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::TXPDO ] )  )
      {
        coe_core::XmlRpcPdo::fromXmlRpcValue( node[ XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::TXPDO ] ], tx_pdo_, "txpdo" );
      }
      
      tx_pdo_.finalize();
      rx_pdo_.finalize();
      
      if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::TXPDO_SIZE ] )  )
      {
        int sz = itia::rutils::toInt ( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::TXPDO_SIZE ] , "txpdo packed size"       ); // exception if error
        tx_pdo_.setPackedBytesLenght( sz );
      }
      if( node.hasMember( XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::RXPDO_SIZE ] )  )
      {
        int sz = itia::rutils::toInt ( node, XmlRpcModule::KeysId[ XmlRpcModule::KeysCode::RXPDO_SIZE ] , "rxpdo packed size"       ); // exception if error
        rx_pdo_.setPackedBytesLenght( sz );
      }
    }

    configuration_sdo_.finalize();
    configurated_pdo_sdo_ = true;
  }
  catch( XmlRpc::XmlRpcException& e )
  {
    ROS_ERROR("%s XmlRpcException: %s", HDR.c_str(), e.getMessage().c_str() );
    return false;
  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s Exception: %s", HDR.c_str(),  e.what() );
    return false;
  }
  
  return configurated_pdo_sdo_;

}
 
bool ModuleDescriptor::connectHandles (  )
{
  try
  {
    for( auto & e : axis_command_     ) e.second.entry  = rx_pdo_.subindex( e.second.pdo_subindex  - 1 );
    for( auto & e : axis_feedback_    ) e.second.entry  = tx_pdo_.subindex( e.second.pdo_subindex - 1 );
    
    for( auto & e : analog_outputs_   ) e.second.entry  = rx_pdo_.subindex( e.second.pdo_subindex - 1 );
    for( auto & e : analog_inputs_    ) e.second.entry  = tx_pdo_.subindex( e.second.pdo_subindex - 1 );
    
    for( auto & e : digital_outputs_  ) e.second.entry  = rx_pdo_.subindex( e.second.pdo_subindex - 1 );
    for( auto & e : digital_inputs_   ) e.second.entry  = tx_pdo_.subindex( e.second.pdo_subindex - 1 );

    for( auto & e : word_outputs_     ) e.second.entry  = rx_pdo_.subindex( e.second.pdo_subindex - 1 );
    for( auto & e : word_inputs_      ) e.second.entry  = tx_pdo_.subindex( e.second.pdo_subindex - 1 );

    for( auto & e : axis_command_     ) ROS_DEBUG(" Axis Command  Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
    for( auto & e : axis_feedback_    ) ROS_DEBUG(" Axis Feedback Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
    
    for( auto & e : analog_outputs_   ) ROS_DEBUG(" Axis AO       Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
    for( auto & e : analog_inputs_    ) ROS_DEBUG(" Axis AI       Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
    
    for( auto & e : digital_outputs_  ) ROS_DEBUG(" Axis DO       Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
    for( auto & e : digital_inputs_   ) ROS_DEBUG(" Axis DI       Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
    
    for( auto & e : word_outputs_     ) ROS_DEBUG(" Word Output   Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
    for( auto & e : word_inputs_      ) ROS_DEBUG(" Word Input    Handle '%s' -> '%s'", e.first.c_str(), e.second.entry->to_string().c_str());
      
    connected_ = true;    
  }
  catch( XmlRpc::XmlRpcException& e )
  {
    ROS_ERROR("%s XmlRpcException: %s", HDR.c_str(),  e.getMessage().c_str() );
    return false;
  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s Exception: %s", HDR.c_str(), e.what() );
    return false;
  }

  return connected_;
}

bool ModuleDescriptor::initNodeCoeConfigurationFromSoem( const uint16_t iSlave, std::string slave_name, bool support_sdoca )
{

  std::string module_identifier;
  std::regex allowed_chars_in_name("^[A-Za-z]");

  module_identifier = slave_name;
  if(!std::regex_match(module_identifier,allowed_chars_in_name))
  {
    module_identifier = std::regex_replace(module_identifier, std::regex(R"([^A-Za-z\d])"), "");
  }
  std::string::iterator end_pos = std::remove(module_identifier.begin(), module_identifier.end(), ' ');
  module_identifier.erase(end_pos, module_identifier.end());
  
  module_identifier += "_" + std::to_string( iSlave );
  
  address_              = iSlave;
  model_                = module_identifier;
  support_sdoca_        = support_sdoca;
  enable_dc_           = false;
  configurated_pdo_sdo_ = false;
  
  return true;
}


bool ModuleDescriptor::updateROSParamServer ( )
{
  XmlRpc::XmlRpcValue node;
  XmlRpc::XmlRpcValue root( nh_.getNamespace()+"/"+root_param_ );
  if( !root.hasMember( param_label_ ) )
  {
    XmlRpcModule::toXmlRpcValue( *this, node  );
  }
  else
  {
    node = root[ param_label_ ];
    XmlRpcModule::toXmlRpcValue( *this, node  );
  }
  
  nh_.setParam( nh_.getNamespace()+"/"+root_param_+"/"+param_label_, node );
  
  return true;
}

const size_t   ModuleDescriptor::sizeInputs( ) const
{
  return tx_pdo_.nBytes(true);
}

const size_t   ModuleDescriptor::sizeOutputs( ) const
{
  return rx_pdo_.nBytes(true);
}

void ModuleDescriptor::updateInputs ( const uint8_t* inputs, bool prepended_time )
{
  if( tx_pdo_.nEntries( ) )
  {
    tx_pdo_.update( inputs, prepended_time );
  }
  
}

void ModuleDescriptor::updateOutputs( const uint8_t* outputs, bool prepended_time )
{
  if( rx_pdo_.nEntries( ) )
  {
    assert( outputs );
    rx_pdo_.update( outputs, prepended_time );
  }
}
  
const std::string ModuleDescriptor::to_string( const std::string what )
{
  std::stringstream ret; 
  ret << getIdentifier() << "\n";
  if( (what=="input") || (what=="tx_pdo") || (what=="txpdo") || (what=="all") || (what=="input-output") || (what=="both" ))
  {
    ret << tx_pdo_.to_string();
  }
  
  if( (what=="output") || (what=="rx_pdo") || (what=="rxpdo") || (what=="all") || (what=="input-output") || (what=="both" ))
  {
    ret << rx_pdo_.to_string();
  }
  return ret.str();
}

}

