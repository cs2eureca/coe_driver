/**
 *
 * @file coe_utilities.h
 * @brief FIle with some utility for the management of the Can Over Ethercat protocol
 *
 */
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <XmlRpcException.h>
#include <soem/ethercat.h>
#include <ros/ros.h>
#include <coe_core/coe_sdo.h>
#include <coe_driver/coe_shared_memory.h>
#include <coe_driver/coe_shared_memory.h>


namespace coe_driver 
{

/**
 * 
 * 
 * 
 * 
 * 
 */
bool PdoSharedMemory::init(const ModuleDescriptorPtr& module)
{
  try
  {
    bool ok = sdo_assignement_ == ECT_SDO_RXPDOASSIGN 
            ? module->getRxPdo().npdo( false ) > 0 
            : ( sdo_assignement_ == ECT_SDO_TXPDOASSIGN 
              ? module->getTxPdo().npdo( false ) > 0 
              : false );
    if( !ok) 
    {
      ROS_FATAL("Something wrong happen in the preliminary check (%d/%d/%d - %zu/%zu) . Initialization mismatching. Abort", sdo_assignement_, ECT_SDO_RXPDOASSIGN, ECT_SDO_TXPDOASSIGN, module->getRxPdo().npdo( false ), module->getTxPdo().npdo( false ) );
      return false;
    }
    
    name_                     = sdo_assignement_ == ECT_SDO_RXPDOASSIGN ? module->getSoemOutputId()       : module->getSoemInputId();
    dim_with_time_prepended_  = sdo_assignement_ == ECT_SDO_RXPDOASSIGN ? module->getRxPdo().nbytes(true) : module->getTxPdo().nbytes(true);
    dim_data_                 = sdo_assignement_ == ECT_SDO_RXPDOASSIGN ? module->getRxPdo().nbytes(false): module->getTxPdo().nbytes(false);

    if( access_mode_ == CREATE )
    {
      ROS_INFO("Remove mutex with name '%s' (if exists)", name_.c_str());
      boost::interprocess::named_mutex::remove( name_.c_str() );
      
      ROS_INFO("Remove memory with name '%s' (if exists)", name_.c_str());
      boost::interprocess::shared_memory_object::remove( name_.c_str() );

      ROS_INFO("Create memory with name '%s'", name_.c_str());
      pdo_shared_memory_ = boost::interprocess::shared_memory_object( boost::interprocess::create_only
                                                                    , name_.c_str()
                                                                    , boost::interprocess::read_write);

      ROS_INFO("Trunc memory '%s' at dimension %zu ", name_.c_str(), dim_with_time_prepended_);
      pdo_shared_memory_.truncate( dim_with_time_prepended_ );
      pdo_shared_map_ = boost::interprocess::mapped_region( pdo_shared_memory_, boost::interprocess::read_write);

      ROS_INFO("Reset Memory '%s' at 0 (size: %zu)", name_.c_str(), pdo_shared_map_.get_size() );
      std::memset(pdo_shared_map_.get_address(), 0, pdo_shared_map_.get_size());
      
      mutex_.reset( new  boost::interprocess::named_mutex( boost::interprocess::create_only, name_.c_str() ) );
    }
    else
    {
      ROS_INFO("Access to memory with name '%s'", name_.c_str());
      pdo_shared_memory_ = boost::interprocess::shared_memory_object ( boost::interprocess::open_only
                                                                      , name_.c_str()
                                                                      , boost::interprocess::read_write);

      ROS_INFO("Mapping to memory with name '%s'", name_.c_str());
      pdo_shared_map_ = boost::interprocess::mapped_region( pdo_shared_memory_, boost::interprocess::read_write);


      ROS_INFO("Getting the named muxex '%s'", name_.c_str());
      mutex_.reset( new  boost::interprocess::named_mutex( boost::interprocess::open_only, name_.c_str() ) );

      ROS_INFO("Ready to get data from '%s' shm ", name_.c_str());
    }
    
  }
  catch(boost::interprocess::interprocess_exception &e)
  {
    ROS_ERROR_STREAM( "In processing module '" << module->getIdentifier() << "' got the error: " << e.what() ) ;
    return false;
  }
  catch( std::exception& e )
  {
    ROS_ERROR_STREAM( "In processing module '" << module->getIdentifier() << "' got the error: " << e.what() );
    return false;
  }

 return true;
}


bool PdoSharedMemory::init(const std::string& identifier )
{
  try
  {
    if( access_mode_ == CREATE )
    {
      ROS_FATAL("Error. The initialization using only the identifier can be done only for 'OPEN' access mode. Abort.");
      return false;

    }
    name_  = identifier + ( sdo_assignement_ == ECT_SDO_RXPDOASSIGN ? "_rxpdo" : "_txpdo" );
    
    ROS_INFO("Access to memory with name '%s'", name_.c_str());
    pdo_shared_memory_ = boost::interprocess::shared_memory_object ( boost::interprocess::open_only
                                                                    , name_.c_str()
                                                                    , boost::interprocess::read_write);

    ROS_INFO("Mapping to memory with name '%s'", name_.c_str());
    pdo_shared_map_ = boost::interprocess::mapped_region( pdo_shared_memory_, boost::interprocess::read_write);
    
    
    ROS_INFO("Getting the named muxex '%s'", name_.c_str());
    mutex_.reset( new  boost::interprocess::named_mutex( boost::interprocess::open_only, name_.c_str() ) );


    dim_with_time_prepended_  = pdo_shared_map_.get_size();
    dim_data_                 = pdo_shared_map_.get_size()-sizeof(double);

    assert( dim_data_ > 0 );
    
  }
  catch(boost::interprocess::interprocess_exception &e)
  {
    ROS_ERROR_STREAM( "In processing module '" << identifier << "' got the error: " << e.what() ) ;
    return false;
  }
  catch( std::exception& e )
  {
    ROS_ERROR_STREAM( "In processing module '" << identifier << "' got the error: " << e.what() );
    return false;
  }

 return true;
}



void PdoSharedMemory::operator<<(const coe_core::Pdo& pdo)
{
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock( *mutex_ );
  std::memcpy ( pdo_shared_map_.get_address(), pdo.data(true), pdo_shared_map_.get_size() ); 
  lock.unlock();
  
}
void PdoSharedMemory::operator>>( coe_core::Pdo& pdo )
{
  static uint8_t _buffer[1024] = {0};
  assert( pdo_shared_map_.get_size() < 1024 );
  
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock( *mutex_ );
  std::memcpy(pdo_shared_map_.get_address(), _buffer, pdo_shared_map_.get_size() ); 
  lock.unlock();
  
  pdo.update( _buffer,true);
  
}

void PdoSharedMemory::operator<<(const uint8_t* buffer)
{
  static uint8_t _buffer[1024] = {0};
  assert( (pdo_shared_map_.get_size() + (dim_with_time_prepended_ - dim_data_) )< 1024 );
  
  // time to prepend in local buffer
  double act_time = ros::WallTime::now().toSec();
  std::memcpy ( _buffer, &act_time , dim_with_time_prepended_ - dim_data_ );
  
  // from buffer to local buffer
  std::memcpy ( _buffer + (dim_with_time_prepended_ - dim_data_), buffer, dim_data_ ); 
  
  // from local buffer to shared memory
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock( *mutex_ );
  std::memcpy ( pdo_shared_map_.get_address(), _buffer, pdo_shared_map_.get_size() ); 
  lock.unlock();
}


void PdoSharedMemory::operator>>( uint8_t* buffer )
{
  static uint8_t _buffer[1024] = {0};
  assert( (pdo_shared_map_.get_size() + (dim_with_time_prepended_ - dim_data_) )< 1024 );
  
  // from shared memory to local buffer
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock( *mutex_ );
  std::memcpy(_buffer, pdo_shared_map_.get_address(), pdo_shared_map_.get_size() ); 
  lock.unlock();
  // from local buffer to buffer 
  std::memcpy( buffer, _buffer+(dim_with_time_prepended_ - dim_data_), dim_data_ ); 
}



PdoSharedMemory::~PdoSharedMemory()
{
  if( access_mode_ == CREATE )
  {
    boost::interprocess::shared_memory_object::remove(name_.c_str());
  }
}


}
