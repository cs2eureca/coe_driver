/**
 *
 * @file coe_utilities.h
 * @brief FIle with some utility for the management of the Can Over Ethercat protocol
 *
 */
#include <boost/interprocess/shared_memory_object.hpp>
#include <XmlRpcException.h>
#include <coe_core/coe_sdo.h>
#include <coe_driver/modules/coe_network_descriptor.h>
#include <coe_driver/modules/coe_network_descriptor_xmlrpc.h>
#include <itia_rutils/itia_rutils.h>
#include <itia_rutils/itia_rutils_xmlrpc.h>


namespace coe_driver 
{
 
bool NetworkDescriptor::initNetworkNames( )
{
  
  try 
  {
    ROS_INFO("[%s%s%s] %sExtract the coe configuration information from ros param server ", BOLDMAGENTA, "START", RESET, BOLDYELLOW  );
  
    XmlRpc::XmlRpcValue module_list; 
    itia::rutils::extractParam( nh_, param_root_+std::string("/") + "adapter"         , adapter_   );
    itia::rutils::extractParam( nh_, param_root_+std::string("/") + "operational_time", operational_time_ );
    itia::rutils::extractParam( nh_, param_root_+std::string("/") + "module_list"     , module_list );

    
    for( int i=0; i<module_list.size(); i++ )
    {
      XmlRpc::XmlRpcValue module = module_list[i];
      std::string label   = itia::rutils::toString( module,  coe_driver::XmlRpcNetwork::KeysId[ coe_driver::XmlRpcNetwork::LABEL    ] );
      int         addr    = itia::rutils::toInt   ( module,  coe_driver::XmlRpcNetwork::KeysId[ coe_driver::XmlRpcNetwork::ADDRESS  ] );
      bool        mapped  = itia::rutils::toBool  ( module,  coe_driver::XmlRpcNetwork::KeysId[ coe_driver::XmlRpcNetwork::MAPPED   ] );
      bool        standard= itia::rutils::toBool  ( module,  coe_driver::XmlRpcNetwork::KeysId[ coe_driver::XmlRpcNetwork::DEFAULT  ] );
      
      if( mapped )
      {
        if( module_addresses_map_.count( addr ) > 0 )
        {
          ROS_ERROR("More than one module has the same address '%d'. Abort.", addr );
          return false;
        }
        module_addresses_map_[ addr ] = label;
        module_default_map_[ addr ] = standard;
      }
      
    }
    ROS_INFO("[%s%s%s] %sExtract the coe configuration information from ros param server ", BOLDGREEN, "  OK ", RESET, BOLDYELLOW  );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("--------------------------");
    ROS_FATAL("%s",e.what());
    ROS_FATAL("--------------------------");
    return false;
  }
  
  
  return true;
}


std::vector<int> NetworkDescriptor::getAddresses  ( )  const { 
  std::vector<int> mapped_addresses;
  mapped_addresses.resize( module_addresses_map_.size() );
    std::transform(module_addresses_map_.begin(),module_addresses_map_.end(),mapped_addresses.begin(),[](const std::pair<int,std::string>& m){ return m.first; } );
  
  return mapped_addresses; 
}

std::vector<std::string> NetworkDescriptor::getNodeUniqueIDs  ( )  const { 
  std::vector<std::string> mapped_names;
  mapped_names.resize( module_addresses_map_.size() );
  std::transform(module_addresses_map_.begin(),module_addresses_map_.end(),mapped_names.begin(),[](const std::pair<int,std::string>& m){ return uniqueId(m.second,m.first); } );
  
  return mapped_names; 
}  

std::map<int,std::string> NetworkDescriptor::getAddressLabelsMap  ( )  const { 
  std::map<int,std::string> sorted_names;
  for( auto const & module : module_addresses_map_ )
  {
    sorted_names[ module.first ]  = module.second;
  }
  
  return sorted_names; 
} 

std::map<int,std::string> NetworkDescriptor::getAddressUniqueIdMap  ( )  const { 
  std::map<int,std::string> sorted_names;
  for( auto const & module : module_addresses_map_ )
  {
    sorted_names[ module.first ]  = uniqueId(module.second, module.first);
  }
  
  return sorted_names; 
} 


bool NetworkDescriptor::hasDefaultConfiguration( int addr ) const
{
  auto it = module_default_map_.find( addr );
  if( it != module_default_map_.end() )
  {
    return module_default_map_.at( addr );
  }
  return false;
}

bool NetworkDescriptor::checkAddress ( int addr )              const 
{ 
  return module_addresses_map_.find( addr ) != module_addresses_map_.end(); 
  return false; 
}

int NetworkDescriptor::checkModuleName   ( const std::string& id, const bool verbose ) const 
{ 
  int ret = -1;
  
  auto it = std::find_if(module_addresses_map_.begin(),module_addresses_map_.end(),[&id](const std::pair<int,std::string>& m){  return uniqueId(m.second,m.first ) == id; } );
  
  ret = ( it != module_addresses_map_.end() ) ? it->first : -1;
  
  if( (ret==-1) && verbose )
  {
    ROS_WARN("[checkModuleName] Requested: '%s', while the name available are:", id.c_str() );
    for( auto const & s : getNodeUniqueIDs() )
    {
      std::cout << "- " << s << std::endl;
    }
    
  }
  return ret; 
}




}
