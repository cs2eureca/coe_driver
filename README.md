![Alt text](media/logo-eureca-3.png)

# COE DRIVER #

The package has been developed within the scope of the project [EURECA](www.cleansky-eureca.eu/), funnded from the [Clean Sky](www.cleansky.eu) Joint Undertaking under the [European Union’s Horizon 2020]](https://ec.europa.eu/programmes/horizon2020/)  research and innovation programme under grant agreement nº 738039

> _The EURECA project framework is dedicated to innovate the assembly of aircraft interiors using advanced human-robot collaborative solutions. A pool of devices/frameworks will be deployed for teaming up with human operators in a human-centred assistive environment. The main benefits are the substantial improvement of ergonomics in workloads, the increase in the usability level of assembly actions, the digitalization of procedures with logs and visuals for error prevention through dedicated devices. Solutions are peculiarly designed for addressing both the working conditions and the management of the cabin-cargo installation process, such as limited maneuvering space, limited weight allowed on the cabin floor, reducing lead time and recurring costs. With this aim, EURECA will bring together research advancements spanning across design, manufacturing and control, robotized hardware, and software for the specific use-cases in the cabin-cargo final assembly._



### CanOpen over Ethercat ###

The CanOpen over Ethercat, [CoE](https://www.can-cia.org/fileadmin/resources/documents/proceedings/2005_rostan.pdf) hereafter, is among the most vesatile and used can in the industrial field, thanks to its intrinsic powerful architecture.  The packes should be considered as an extension with planty of utilities of the FOSS project Simple Open EtherCAT Master [SOEM](https://github.com/ros-industrial/ethercat-soem).

Tha package has been designed in order to control the new empowering collaborative robot deployed in EURECA 

![Alt text](media/immagine-progetto-small.jpg)


The package is structured in many ROS packages, and specifically:

Package Name | Description
---:|:---
	 [coe_soem_utilites](https://bitbucket.org/iras-ind/coe_soem_utilities/src/master/) | a set of functionalities that ease the development of SOEM based applciations
	 [coe_core](https://bitbucket.org/iras-ind/coe_core/src/master/) | a set of libraries that implements the most important CanOpen structures, and it provides some utilities to deploy DS402 (motor drivers) applications 
	 [coe_driver](https://bitbucket.org/iras-ind/coe_driver/src/master/) |  a ROS based node that deploys the SOEM  exploting a shared-mameory mechanism
	 [coe_hw_plugins](https://bitbucket.org/iras-ind/coe_hw_plugins/src/master/) | the plugins (based on pluginlib) that allows an easy deployment of client applications 


##coe_driver Design ##

###Configuration ###

The configuration is based on one __yaml__file, split in two main parts:

   - the map of the netowrk, i.e., the addresses and the label for each note
   - a description of all the configurations for each module
   
The list of the modules should list all the nodes in the network. However, if some nodes are not used, or they are non-functional nodes (e.g., the couplers like the Bechkoff EL1100, or the power supply etc), it is possible to skip the configuration adding __'mapped: false'__ to the corresponding node.

For ech node is then possible to configure all the driver functionalities.

```yaml

coe:
  adapter: enp0s31f6 #enp2s0 #eth1
  operational_time: 0.001
  
  module_list:
    - {address:  4, default: 0, label: 'ElmoDriver', mapped: true}
    - {address:  2, default: 0, label: 'ElmoDriver', mapped: true}
    - {address:  3, default: 0, label: 'ElmoDriver', mapped: true}
    - {address:  1, default: 0, label: 'ElmoDriver', mapped: true}
    
 ElmoDriver:
    type: "coe/ElmoGoldWhistle"
    description: TODO
    model: M0000009aI00030924
    enable_dc: 1
    sdo_complete_access: false
    watchdog_decimation: 50
    loop_rate_decimation: 1
    diagnostic_decimation: 50
    axis_command:
    - {name: "torque", scale: 1, offset: 0.0, pdo_subindex: 2 }
    - {name: "position", scale: 0.5e-3, offset: 0.0, pdo_subindex: 3 }
    - {name: "velocity", scale: 0.5e-3, offset: 0.0, pdo_subindex: 6 }
        
    axis_feedback:
    - {name: "torque",    scale: 1, offset: 0.0, pdo_subindex: 4}
    - {name: "position",  scale: 0.5e-3, offset: 0.0, pdo_subindex: 2}
    - {name: "velocity",  scale: 0.5e-3, offset: 0.0, pdo_subindex: 3}
        
    digital_inputs:
    - {name: "DI1", pdo_subindex: 5, bit: 0 }
    - {name: "DI2", pdo_subindex: 5, bit: 1 }
    - {name: "DI3", pdo_subindex: 5, bit: 2 }
    - {name: "DI4", pdo_subindex: 5, bit: 3 }
    - {name: "DI5", pdo_subindex: 5, bit: 4 }
    - {name: "DI6", pdo_subindex: 5, bit: 5 }
    - {name: "DI7", pdo_subindex: 5, bit: 6 }
    - {name: "DI8", pdo_subindex: 5, bit: 7 }
    
    digital_outputs:
    - {name: "DI1", pdo_subindex: 7, bit: 0 }
    - {name: "DI2", pdo_subindex: 7, bit: 1 }
    - {name: "DI3", pdo_subindex: 7, bit: 2 }
    - {name: "DI4", pdo_subindex: 7, bit: 3 }
    - {name: "DI5", pdo_subindex: 7, bit: 4 }
    - {name: "DI6", pdo_subindex: 7, bit: 5 }
    - {name: "DI7", pdo_subindex: 7, bit: 6 }
    - {name: "DI8", pdo_subindex: 7, bit: 7 }
    
    rxpdo:
    - {pdo_subindex: 1, value_id: Controlword,      value_index: 0x160A, value_subindex: 1, value_type: UNSIGNED16}
    - {pdo_subindex: 2, value_id: Target torque,    value_index: 0x160C, value_subindex: 1, value_type: INTEGER16}
    - {pdo_subindex: 3, value_id: Target position,  value_index: 0x160F, value_subindex: 1, value_type: INTEGER32}
    - {pdo_subindex: 4, value_id: Velocity Offset,  value_index: 0x1617, value_subindex: 1, value_type: INTEGER32}
    - {pdo_subindex: 5, value_id: Torque Offset,    value_index: 0x1618, value_subindex: 1, value_type: INTEGER16}
    - {pdo_subindex: 6, value_id: Target Velocity,  value_index: 0x161C, value_subindex: 1, value_type: INTEGER32}
    - {pdo_subindex: 7, value_id: Digital Output,   value_index: 0x161D, value_subindex: 1, value_type: UNSIGNED32}
    
    txpdo:
    - {pdo_subindex: 1, value_id: Statusword,                     value_index: 0x1A0A, value_subindex: 1, value_type: UNSIGNED16}
    - {pdo_subindex: 2, value_id: Position actual value,          value_index: 0x1A0E, value_subindex: 1, value_type: INTEGER32}
    - {pdo_subindex: 3, value_id: Velocity sensor actual value,   value_index: 0x1A0F, value_subindex: 1, value_type: INTEGER32}
    - {pdo_subindex: 4, value_id: Torque actual value,            value_index: 0x1A12, value_subindex: 1, value_type: INTEGER16}
    - {pdo_subindex: 5, value_id: Digital inputs,                 value_index: 0x1A1C, value_subindex: 1, value_type: UNSIGNED32}
    
    sdo:
    - {value_id: SUPPORTED DRIVE MODES,             value_index: 0x6502, value_subindex: 0, value_type: UNSIGNED32, value_access: read }   
    - {value_id: ABORT CONNECTION OPTION CODE,      value_index: 0x6007, value_subindex: 0, value_type: INTEGER16,  value_access: write, value: 0 }
    
    - {value_id: DISABLE OPERATION OPTION CODE,     value_index: 0x605C, value_subindex: 0, value_type: INTEGER16,  value_access: write, value: 0 }
    - {value_id: QUICK STOP OPTION CODE,            value_index: 0x605A, value_subindex: 0, value_type: INTEGER16,  value_access: write, value: 0 }
    - {value_id: HALT OPTION CODE,                  value_index: 0x605D, value_subindex: 0, value_type: INTEGER16,  value_access: write, value: 1 }
    
    - {value_id: HOMING SPEED SEARCH SWITCH ZERO,   value_index: 0x6099, value_subindex: 1, value_type: UNSIGNED32, value_access: write, value: 100}
    - {value_id: HOMING SPEED SEARCH ZERO,          value_index: 0x6099, value_subindex: 2, value_type: UNSIGNED32, value_access: write, value: 100}
    - {value_id: HOMING ACCELERATION,               value_index: 0x609A, value_subindex: 0, value_type: UNSIGNED32, value_access: write, value: 10000}
    - {value_id: HOMING OFFSET,                     value_index: 0x607C, value_subindex: 0, value_type: INTEGER32,  value_access: write, value:  0}
    - {value_id: HOMING METHOD,                     value_index: 0x6098, value_subindex: 0, value_type: INTEGER8,   value_access: write, value:  33}
    
    - {value_id: MIN POSITION RANGE LIMIT,          value_index: 0x607B, value_subindex: 1, value_type: INTEGER32, value_access: write, value: -400000}
    - {value_id: MAX POSITION RANGE LIMIT,          value_index: 0x607B, value_subindex: 2, value_type: INTEGER32, value_access: write, value:  400000}

    - {value_id: MIN SOFTWARE LIMIT,                value_index: 0x607D, value_subindex: 1, value_type: INTEGER32, value_access: write, value: -400000}
    - {value_id: MAX SOFTWARE LIMIT,                value_index: 0x607D, value_subindex: 2, value_type: INTEGER32, value_access: write, value:  400000}
        
    - {value_id: POSITION WINDOW,                   value_index: 0x6067, value_subindex: 0, value_type: UNSIGNED32, value_access: write, value:  10}
    
    - {value_id: FOLLOWING ERROR WINDOW,            value_index: 0x6065, value_subindex: 0, value_type: UNSIGNED32, value_access: write, value:  1000000}
    - {value_id: FOLLOWING_ERROR_WINDOW_TIME,       value_index: 0x6066, value_subindex: 0, value_type: UNSIGNED16, value_access: write, value:  1000000}
    
    - {value_id: VELOCITY WINDOW,                   value_index: 0x606D, value_subindex: 0, value_type: UNSIGNED16, value_access: write, value:  10}
    - {value_id: VELOCITY THRESHOLD,                value_index: 0x606E, value_subindex: 0, value_type: UNSIGNED16, value_access: write, value:  1000000}
    
    
```

The configuration core is to wirte the correct PDO assignement, i.e., to compose correctly the communication dictionary.

The __coe_driver__ translates the configuration in many different _shared_memory_ file descriptor under _/dev/shmem_. There are two file descriptors for each node, one for the incoming data and one for the outcoming data.

The refresh time can be accordingl set on the configuration time.

## Launch ##

The typical launch file is the following:

```xml
<launch>
  <param name="use_sim_time" type="bool" value="false" /> 

  <arg name="debug" value="false" />

  <arg name="launch-prefix"   value="gdb -ex run --args" if="$(arg debug)" />
  <arg name="launch-prefix"   value=""         unless="$(arg debug)" />
  
  <node launch-prefix="$(arg launch-prefix)" pkg="coe_driver" name="eureca_master" type="coe_driver_node" output="screen" >
    <rosparam command="load" file="$(find eureca_popeye_coe)/cfg/coe_config_driver.yaml"  />
    <rosparam command="load" file="$(find eureca_popeye_coe)/cfg/coe_config_elmo.yaml"    />
    <param    name="diagnostic_period_parameter" value="100" />
  </node>
  
  <node pkg="diagnostic_aggregator" type="aggregator_node" name="diag_agg" output="screen" >
    <rosparam command="load"  file="$(find eureca_popeye_coe)/cfg/analistics.yaml" />
  </node>


</launch>

```

In addition to the node that deploys the __coe_master__, the __diagnostic_aggregator__ is launched to gather all the errors and warning raised by the automation network.

	__NOTE__: The master should be launched as super user. Since in ROS such operation is not straightfoward, plase enabled the __su__ user, and then launch the node as __su__
	
###Behavior ###

The node cyclically update each shared file descriptor. Futhermore, each connection to the shared memory is tracked and, a pair-wise mechanism is implemented. 

The minimum cycle time on a i7-5500 desktop with 4GB of RAM, is about 0.1ms, with a standard vanilla kenrel (ubuntu 16.04 tested)
	
The __coe_driver__ can be compiled also for RT-Linux and Xenomai without any issue.

### Connections ###

Tha package provides an abstract class __CoeHwPlugin__ that aims at easing the deployment of the __client__ side of a CoE application.

Furthermore, the __coe_driver__ main node provides the following services and topics:

```yaml

__TO BE DONE__

```

### The CoeHwPlugin ###


The package provides a pure virtual class called __CoeHwPlugin__ that has been designed to simplify the devlopment of proper plugin for the client applications.

The idea is that each client is an implementation of a set of different plugins, each implementing the management of a single node of the CoE network of automation (e.g., a sensor, a dirver, etc)

The plugin interface offers:

* the parsing of the configuration file (see __coe_master__)
* the access to the shared_memory, with the binding procedure
* the plugin for the filtering and parsing of the errors



##Miscellaneous ##

###Dependencies ###

The only no-ROS dependency is [SOEM](https://github.com/OpenEtherCATsociety/SOEM).

WHile, the ROS depndenies are:

```xml
  <build_depend>message_generation</build_depend>
  <run_depend>message_runtime</run_depend>

  <buildtool_depend>catkin</buildtool_depend>

  <build_depend>coe_core</build_depend>
  <run_depend>coe_core</run_depend>
  
  <!-- set of real-time libraries deployed by CNR-ITIA -->
  <build_depend>itia_rtutils</build_depend>
  <run_depend>itia_rtutils</run_depend>

  <!-- set of  libraries deployed by CNR-ITIA to ease the use of safe-threaded circular buffers -->
  <build_depend>itia_butils</build_depend>
  <run_depend>itia_butils</run_depend>

  <!-- set of  libraries deployed by CNR-ITIA to ease the use of safe-threaded file logging -->
  <build_depend>itia_futils</build_depend>
  <run_depend>itia_futils</run_depend>

  <!-- set of  libraries deployed by CNR-ITIA to exploti some ROS mechaism -->
  <build_depend>itia_rutils</build_depend>
  <run_depend>itia_rutils</run_depend>

  <build_depend>roscpp</build_depend>
  <run_depend>roscpp</run_depend>
  
    <!-- All the diagnostic is performed following the standard ROS implementation-->
  <build_depend>diagnostic_updater</build_depend>
  <run_depend>diagnostic_updater</run_depend>
  
  <build_depend>diagnostic_aggregator</build_depend>
  <run_depend>diagnostic_aggregator</run_depend>
  
      <!-- Set of libraries to ese the use of SOEM -->
  <build_depend>coe_soem_utilities</build_depend>
  <run_depend>coe_soem_utilities</run_depend>
  
  <!-- ROS project to ease the deployment of Finite State Machines -->
  <build_depend>driver_base</build_depend>
  <run_depend>driver_base</run_depend>
  
    <!--  The pluginlib mechannism has been selected as the the tool to deploy multi client applications -->
  <build_depend>pluginlib</build_depend>
  <run_depend>pluginlib</run_depend>
```
  
### Contribution guidelines ###

The __coe_driver__ implements a simple mapping of a generic CoE network in a typed shared memory.

Two design points are relevant:

* The parametrization of the network is easily done using a __yaml__ file,
* The __coe_driver__ is multi-client, and it supports the _pluginlib_ to the easy implementation of the client application.

### Who do I talk to? ###

* Nicola Pedrocchi, [nicola.pedrocchi@stiima.cnr.it](mailto:nicola.pedrocchi@stiima.cnr.it) 