
#ifndef __coe__ros__shared_memeory__h__
#define __coe__ros__shared_memeory__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>

#include <tuple>
#include <ros/ros.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_driver/coe_network_descriptor.h>

namespace coe_driver 
{

class PdoSharedMemory
{
public:
  
  typedef std::shared_ptr< PdoSharedMemory >  Ptr;
  
  enum ShmAccessMode { CREATE, OPEN };
  
  PdoSharedMemory(const ModuleDescriptorPtr& module, const ShmAccessMode& mode, const int sdo_assignement )
  : sdo_assignement_( sdo_assignement )
  , access_mode_    ( mode )
  {
    if(!init(module))
      throw std::runtime_error("Error in creation of the shared memory. Exit.");
  }
  PdoSharedMemory(const std::string& identifier, const int sdo_assignement )
  : sdo_assignement_( sdo_assignement )
  , access_mode_    ( OPEN )
  {
    if(!init(identifier))
      throw std::runtime_error("Error in creation of the shared memory. Exit.");
  }
  ~PdoSharedMemory();

  void operator<<( const coe_core::Pdo& pdo );
  void operator>>( coe_core::Pdo& pdo );
  
  void operator<<( const uint8_t* buffer );
  void operator>>( uint8_t* buffer );
  
  size_t getSize( bool prepend_time = true ) const { return prepend_time ? dim_with_time_prepended_ : dim_data_; }
   
private:
  const int                                         sdo_assignement_ ;
  const ShmAccessMode                               access_mode_;
  std::string                                       name_;
  size_t                                            dim_data_;
  size_t                                            dim_with_time_prepended_;
  boost::interprocess::mapped_region                pdo_shared_map_;
  boost::interprocess::shared_memory_object         pdo_shared_memory_;
  std::shared_ptr<boost::interprocess::named_mutex> mutex_;
  
  bool init( const ModuleDescriptorPtr& modules );
  bool init( const std::string& identifier );
};

typedef PdoSharedMemory::Ptr PdoSharedMemoryPtr;


class ModuleSharedMemory
{
public:
  typedef std::shared_ptr< ModuleSharedMemory > Ptr;
  
  const std::string   identifier_;
  PdoSharedMemory     rx_pdo_;
  PdoSharedMemory     tx_pdo_;
  
  ModuleSharedMemory( const ModuleDescriptorPtr& module, const PdoSharedMemory::ShmAccessMode& mode )
  : identifier_(module->getIdentifier() ), rx_pdo_(module, mode, ECT_SDO_RXPDOASSIGN), tx_pdo_(module, mode, ECT_SDO_TXPDOASSIGN)
  {
  }
  
  ModuleSharedMemory( const std::string& identifier )
  : identifier_( identifier ), rx_pdo_(identifier, ECT_SDO_RXPDOASSIGN), tx_pdo_(identifier, ECT_SDO_TXPDOASSIGN)
  {
  }
};

typedef ModuleSharedMemory::Ptr ModuleSharedMemoryPtr;


class ModulesSharedMemory
{
  
public:
  
  typedef std::vector<ModuleSharedMemoryPtr>   List;
  typedef List::iterator                       iterator;
  typedef List::const_iterator                 const_iterator;
  
  
  void                          clear()         { modules_shm_.clear(); }
  iterator                      begin()         { return modules_shm_.begin(); }
  iterator                      end()           { return modules_shm_.end(); }
  
  const_iterator                begin()  const  { return modules_shm_.begin(); }
  const_iterator                end()    const  { return modules_shm_.end(); }
  
  const_iterator                cbegin() const  { return modules_shm_.cbegin(); }
  const_iterator                cend()   const  { return modules_shm_.cend(); }
    
  const ModuleSharedMemoryPtr&  operator[]( const std::string& i ) const  
  { 
    auto const & it = std::find_if( modules_shm_.begin(), modules_shm_.end(), [&i]( ModuleSharedMemoryPtr m){ return i == (m->identifier_); } );
    if( it == modules_shm_.end() )
      throw std::runtime_error( ("Shared memory identifier '"+ i + "' not in the mapped list" ).c_str() );
    return *it;
  }
  ModuleSharedMemoryPtr&        operator[]( const std::string& i )
  { 
    auto it = std::find_if( modules_shm_.begin(), modules_shm_.end(), [&i]( const ModuleSharedMemoryPtr& m){ return i == m->identifier_; } );
    if( it == modules_shm_.end() )
      throw std::runtime_error( ("Shared memory identifier '"+ i + "' not in the mapped list" ).c_str() );
    return *it;
  }
  
  bool insert( ModuleSharedMemoryPtr module_shm ) 
  { 
    for( const ModuleSharedMemoryPtr& module : modules_shm_ )
    {
      if( module->identifier_ == module_shm->identifier_ )
      {
        ROS_ERROR("Module already in the list.");
        return false;
      }
    }
    modules_shm_.push_back(module_shm); 
    return true;
  }
  
private:
  
  List modules_shm_;
  
};



}

#endif
