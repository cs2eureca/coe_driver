#ifndef __COE_DRIVER__DIAGNOSTICS_H__
#define __COE_DRIVER__DIAGNOSTICS_H__

#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>
#include <driver_base/driver.h>


#include <itia_futils/itia_futils.h>
#include <itia_futils/string.h>
#include <itia_rtutils/itia_rtutils.h>
#include <itia_butils/itia_butils.h>

#include <coe_core/coe_sdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_soem_utilities/coe_soem_utilities.h>

namespace coe_driver 
{
  
template<class T>
class BaseDiagnostic
{
protected:
  
  const std::string                                                 message_; 
  const int8_t                                                      level_;
  const int8_t                                                      dim_;
  itia::butils::circ_buffer<std::tuple< std::string, double, T > >  errors_;
  const double                                                      stale_time_;
  
protected:
  
  virtual void parseError(const std::tuple< std::string, double, T>& e, diagnostic_updater::DiagnosticStatusWrapper &stat) = 0;
  
public:
  
  typedef std::shared_ptr< T > Ptr;
  
  BaseDiagnostic ( const std::string&  message
                , int8_t              level
                , const int           buffer_length = 10 ) 
    : message_(message), level_(level), dim_( buffer_length ), errors_( buffer_length ), stale_time_(10)
  { 
    // nothing to do so far
  }
  
  bool push_back( const T& error )
  {
    ros::Time   n = ros::Time::now();
    double      t = n.toSec();
    std::string s = boost::posix_time::to_simple_string( n.toBoost() );
    errors_.push_back( std::make_tuple( s, t, error ) );
    return true;
  }
  
  void clear()
  {
    return errors_.clear();
  }
  
  void coeDiagnostics(diagnostic_updater::DiagnosticStatusWrapper &stat)
  {
    ros::Time   n  = ros::Time::now();
    double      at = n.toSec();
    std::string st = " [" + boost::posix_time::to_simple_string( n.toBoost() ) +"]";
    
    size_t sz = errors_.size();
    if( sz == 0 ) 
    {
      stat.summary(diagnostic_msgs::DiagnosticStatus::OK,  "No errors" + st );
    }
    else 
    {
      bool stale = true;
      for( size_t i=0; i<sz; i++ )
      {
        const std::tuple<std::string, double, T > & e = errors_.get().at(i);
        double et = std::get<1>( e );
        stale &= ( ( at - et ) > stale_time_ );
        
        parseError( e, stat );
      }
      stat.summary( stale ? diagnostic_msgs::DiagnosticStatus::STALE : level_, " Errors (last " + std::to_string( dim_ ) + " items)" + st);
    }
  }
};

/**
 * 
 * 
 * 
 * 
 * 
 * 
 */
class MasterDiagnostic : public BaseDiagnostic< std::string >
{
protected:
  void parseError(const std::tuple< std::string, double, std::string >& e, diagnostic_updater::DiagnosticStatusWrapper &stat)
  {
    stat.add(std::string("Alarm ["+std::get<0>(e)+"]").c_str(), std::get<2>(e).c_str() );
  }
public:
  
  typedef std::shared_ptr< MasterDiagnostic > Ptr;
  
  MasterDiagnostic( const int buffer_length = 10 ) : BaseDiagnostic( "Unspecialized Errors", diagnostic_msgs::DiagnosticStatus::ERROR, buffer_length ){ }
  
};

typedef MasterDiagnostic::Ptr MasterDiagnosticPtr;


/**
 * 
 * 
 * 
 * 
 */
class ModuleDiagnostic : public BaseDiagnostic< ec_errort >
{
private:
  const int                   addr_;
  
protected:
  
  void parseError(const std::tuple< std::string, double, ec_errort >& e, diagnostic_updater::DiagnosticStatusWrapper &stat)
  {
    const ec_errort & Ec = std::get<2>(e);
  
    std::string soem_time = " [" + std::get<0>( e ) + "] [" + coe_soem_utilities::to_string( Ec.Etype )+"]";
    switch (Ec.Etype)
    {
      case EC_ERR_TYPE_SDO_ERROR:     stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s 0x%4.4x.%2.2x:%s", soem_time.c_str(), Ec.Index, Ec.SubIdx, ec_sdoerror2string(Ec.AbortCode) ); break;
      case EC_ERR_TYPE_EMERGENCY:     stat.addf(coe_core::to_string_hex(Ec.ErrorCode          ), "%s manufacturer Specific - See Plugin Diagnostic",soem_time.c_str());                             break;
      case EC_ERR_TYPE_PACKET_ERROR:  stat.addf(coe_core::to_string_hex(Ec.ErrorCode          ), "%s 0x%4.4x:%2.2x, Code TODO PARSER",soem_time.c_str(), Ec.Index, Ec.SubIdx );                     break;
      case EC_ERR_TYPE_SDOINFO_ERROR: stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s 0x%4.4x:%2.2x %s", soem_time.c_str(), Ec.Index, Ec.SubIdx, ec_sdoerror2string(Ec.AbortCode));  break;
      case EC_ERR_TYPE_SOE_ERROR:     stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s IDN 0x%4.4x %s", soem_time.c_str(), Ec.Index, ec_soeerror2string(Ec.AbortCode));               break;
      case EC_ERR_TYPE_MBX_ERROR:     stat.addf(coe_core::to_string_hex(Ec.ErrorCode          ), "%s MBX %s", soem_time.c_str(), Ec.Index, ec_mbxerror2string(Ec.ErrorCode));                       break;
      default:                        stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s Not mapped", soem_time.c_str());                                                               break;
    }
  }
  
public:
  
  typedef std::shared_ptr<ModuleDiagnostic> Ptr;
  
 
  ModuleDiagnostic( const int addr, const int buffer_length = 10 ) 
    : BaseDiagnostic( "Module " + std::to_string(addr), diagnostic_msgs::DiagnosticStatus::ERROR, buffer_length )
    , addr_(addr) 
  {
    // nothing to do so far
  }
  
  bool push_back( ec_errort error )
  {
    if( error.Slave != addr_) 
    {
      return false;
    }
    return  BaseDiagnostic< ec_errort >::push_back(error);
  }
  

};

typedef ModuleDiagnostic::Ptr ModuleDiagnosticPtr;

}

#endif



