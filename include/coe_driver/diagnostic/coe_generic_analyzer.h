/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2009, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef __COE__GENERIC_ANALYZER__H___
#define __COE__GENERIC_ANALYZER__H___



#include <map>
#include <ros/ros.h>
#include <vector>
#include <string>
#include <sstream>
#include <boost/shared_ptr.hpp>
#include <boost/regex.hpp>
#include <pluginlib/class_list_macros.hpp>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>
#include <diagnostic_aggregator/analyzer.h>
#include <diagnostic_aggregator/status_item.h>
#include <diagnostic_aggregator/generic_analyzer_base.h>
#include <XmlRpcValue.h>


#include <itia_butils/itia_butils.h>

namespace coe_driver
{


/*!
 *\brief Returns list of strings from a parameter
 *
 * Given an XmlRpcValue, gives vector of strings of that parameter
 *\return False if XmlRpcValue is not string or array of strings
 */
inline bool getParamVals(XmlRpc::XmlRpcValue param, std::vector<std::string> &output)
{
  XmlRpc::XmlRpcValue::Type type = param.getType();
  if (type == XmlRpc::XmlRpcValue::TypeString)
  {
    std::string find = param;
    output.push_back(find);
    return true;
  }
  else if (type == XmlRpc::XmlRpcValue::TypeArray)
  {
    for (int i = 0; i < param.size(); ++i)
    {
      if (param[i].getType() != XmlRpc::XmlRpcValue::TypeString)
      {
        ROS_ERROR("Parameter is not a list of strings, found non-string value. XmlRpcValue: %s", param.toXml().c_str());
        output.clear();
        return false;
      }

      std::string find = param[i];
      output.push_back(find);
    }
    return true;
  }

  ROS_ERROR("Parameter not a list or string, unable to return values. XmlRpcValue:s %s", param.toXml().c_str());
  output.clear();
  return false;
}

class CoeGenericAnalyzer : public diagnostic_aggregator::GenericAnalyzerBase
{
public:
  /*!
   *\brief Default constructor loaded by pluginlib
   */
  CoeGenericAnalyzer();
  
  virtual ~CoeGenericAnalyzer();

  // Move to class description above
  /*!
   *\brief Initializes GenericAnalyzer from namespace. Returns true if s
   *
   *\param base_path : Prefix for all analyzers (ex: 'Robot')
   *\param n : NodeHandle in full namespace
   *\return True if initialization succeed, false if no errors of 
   */
  virtual bool init(const std::string base_path, const ros::NodeHandle &n);

  /*!
   *\brief Returns true if analyzer will analyze this name
   *
   * This is called with every new item that an analyzer matches.
   * Analyzers should only return "true" if they will report the value of this 
   * item. If it is only looking at an item, it should return false.
   */
  virtual bool analyze(const boost::shared_ptr<diagnostic_aggregator::StatusItem> item);
  /*!
   *\brief Reports current state, returns vector of formatted status messages
   * 
   *\return Vector of DiagnosticStatus messages, with correct prefix for all names.
   */
  virtual std::vector<boost::shared_ptr<diagnostic_msgs::DiagnosticStatus> > report();

  /*!
   *\brief Returns true if item matches any of the given criteria
   * 
   */
  virtual bool match(const std::string name);

private:
  
  typedef std::map<std::string, boost::shared_ptr<diagnostic_aggregator::StatusItem> > MappedStatusItem;
  std::vector<std::string> chaff_; /**< Removed from the start of node names. */
  std::vector<std::string> expected_;
  std::vector<std::string> startswith_;
  std::vector<std::string> contains_;
  std::vector<std::string> name_;
  std::vector<boost::regex> regex_; /**< Regular expressions to check against diagnostics names. */
  

};

}

#include "coe_generic_analyzer_imp.hpp"
    
    
#endif