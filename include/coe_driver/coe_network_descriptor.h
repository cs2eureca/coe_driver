
#ifndef __coe__ros__network_decriptor__h__
#define __coe__ros__network_decriptor__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <tuple>
#include <ros/ros.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_driver/coe_module_descriptor.h>

namespace coe_driver 
{

  
class NetworkDescriptor
{
private:
  
  ros::NodeHandle                             nh_;
  std::string                                 param_root_;
  
  bool                                        autostart_;
  std::string                                 adapter_;
  double                                      operational_time_;
  double                                      watchdog_;
  std::vector< ModuleDescriptorPtr >          modules_;
  std::map<std::string, ModuleDescriptorPtr > named_modules_;
  std::map<int, ModuleDescriptorPtr >         sorted_modules_;
  std::map< std::string, int >                modules_model_map_;
  
public:

  NetworkDescriptor(   )
  {
    // nothing to do so far
  }
  
  void initialize( ros::NodeHandle& nh, const std::string& coe_device_parameter_namespace  )
  {
    nh_ =  nh; 
    param_root_ = coe_device_parameter_namespace;
    adapter_ = "none";
    operational_time_ = -1;
    watchdog_= -1;
  }
  
  NetworkDescriptor( ros::NodeHandle& nh, const std::string& coe_device_parameter_namespace  )
  : nh_( nh ), param_root_(coe_device_parameter_namespace), adapter_("none"), operational_time_(-1), watchdog_(-1)
  {
    // nothing to do so far
  }
  
  bool initNames( );
  bool initNodes( );
  
  const bool&                                       getAutostart        ( )                         const { return autostart_; }
  const std::string&                                getAdapterName      ( )                         const { return adapter_; }
  const double&                                     getOperationalTime  ( )                         const { return operational_time_; }
  const double&                                     getWatchDog         ( )                         const { return watchdog_; }
  const int                                         getNumberOfModules  ( )                         const { return (int)modules_.size(); }
  const std::string&                                getModuleName       ( int addr )                const { return sorted_modules_.at( addr )->getIdentifier(); } 
    
  const ModuleDescriptorPtr                         getModule           ( int addr )                const { return sorted_modules_.at( addr ); }
  ModuleDescriptorPtr                               getModule           ( int addr )                      { return sorted_modules_.at( addr ); }
  
  const ModuleDescriptorPtr                         getModule           ( const std::string& id )   const { return named_modules_.at( id ); }
  ModuleDescriptorPtr                               getModule           ( const std::string& id )         { return named_modules_.at( id ); }
  
  const std::map<std::string,ModuleDescriptorPtr>&  getNameModuleMap    ( )                         const { return named_modules_; }
  std::map<std::string,ModuleDescriptorPtr>&        getNameModuleMap    ( )                               { return named_modules_; }
  
  const std::map<int,ModuleDescriptorPtr>&          getAddressModuleMap ( )                         const { return sorted_modules_; }
  std::map<int,ModuleDescriptorPtr>&                getAddressModuleMap ( )                               { return sorted_modules_; }
  
  const std::vector<ModuleDescriptorPtr>&           getModules          ( )                         const { return modules_; }
  std::vector<ModuleDescriptorPtr>&                 getModules          ( )                               { return modules_; }
  
  
  const std::vector<int>&                           getAddresses        ( )                         const;
  const std::vector<std::string>&                   getNames            ( )                         const;
  const std::map<int,std::string>&                  getAddressNameMap   ( )                         const;
  const std::map<std::string,int>&                  getNamedAddressMap  ( )                         const;
  
  const bool checkAddress ( int addr )              const 
  { 
    for( auto const & it : modules_)
    {
      if( it->getAddress() == addr )
        return true;
    }
    return false; 
  }
  
  const bool checkModuleName   ( const std::string& id, const bool verbose = false ) const 
  { 
    auto it = std::find_if(modules_.begin(), modules_.end(), [&id  ](const ModuleDescriptorPtr& m){return m->getIdentifier()==id;}); 
    bool ret = it != modules_.end();
    if( !ret && verbose )
    {
      ROS_WARN("[checkModuleName] Requested: '%s', while the name available are:", id.c_str() );
      for( auto const & s : getNames() )
      {
        std::cout << "- " << s << std::endl;
      }
      
    }
    return ret; 
  }
  
  
  const bool configDc          ( ) const { auto it = std::find_if(modules_.begin(), modules_.end(), [](const ModuleDescriptorPtr& m){return !m->isDcSupported();}); return it != modules_.end(); }
  const bool configSdoCa       ( ) const { auto it = std::find_if(modules_.begin(), modules_.end(), [](const ModuleDescriptorPtr& m){return !m->isSdoCaSupported();}); return it != modules_.end(); }

};

}

#endif
