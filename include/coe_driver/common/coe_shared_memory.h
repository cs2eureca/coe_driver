
#ifndef __coe__ros__base_shared_memeory__h__
#define __coe__ros__base_shared_memeory__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>

#include <tuple>
#include <ros/ros.h>

#include <itia_futils/itia_futils.h>
#include <itia_rtutils/itia_rtutils.h>
#include <coe_core/coe_utilities.h>


namespace coe_driver 
{

/**
 * @class SharedMemory
 * 
 */
class SharedMemory
{

public:
  typedef std::shared_ptr< SharedMemory >  Ptr;
  
  enum ErrorCode  { NONE_ERROR               =  0
                  , UNMACTHED_DATA_DIMENSION =  1
                  , UNCORRECT_CALL           =  2
                  , WATCHDOG                 =  3 };
                  
  struct SharedMemoryStruct
  {
    
    struct Header
    {
      uint8_t bond_flag_;
      uint8_t rt_flag_;
      double  time_;
    } __attribute__((packed)) header_;
    
    char    buffer[1024];
    SharedMemoryStruct( )
    { 
      clear( );
    }
    void clear( ) 
    {
      header_.bond_flag_ = 0;
      header_.rt_flag_ = 0;
      header_.time_ = 0;
      std::memset( &buffer[0], 0x0, 1024*sizeof(char) );
    }
  };



  
  enum ShmAccessMode { CREATE, OPEN };
  
  SharedMemory(const std::string& identifier, double operational_time, double watchdog_decimation, const ShmAccessMode& mode, const size_t dim );
  SharedMemory(const std::string& identifier, double operational_time, double watchdog_decimation );
  ~SharedMemory();
  
  bool      isHardRT ( );
  bool      setHardRT( );
  bool      setSoftRT( );
  
  bool      isBonded ( );
  bool      bond     ( );
  bool      breakBond( );
  ErrorCode update   ( const uint8_t* buffer, const double time, const size_t& n_bytes );
  ErrorCode flush    ( uint8_t* buffer, double* time, double* latency_time, const size_t& n_bytes );
  
  size_t      getSize( bool prepend_header ) const;
  std::string getName()                      const;
  
  std::string to_string( ErrorCode err );
  
  void        dump( SharedMemoryStruct* buffer )
  {
    return getSharedMemoryStruct(buffer);
  }
  
protected:
  
  const ShmAccessMode                               access_mode_;
  const double                                      operational_time_;
  const double                                      watchdog_;
  
  const std::string                                 name_;
  size_t                                            dim_data_;
  size_t                                            dim_with_header_;
  boost::interprocess::mapped_region                shared_map_;
  boost::interprocess::shared_memory_object         shared_memory_;
  std::shared_ptr<boost::interprocess::named_mutex> mutex_;
  double                                            start_watchdog_time_;
  double                                            time_prev_;
  
  size_t                                            bond_cnt_;
  bool                                              bonded_prev_;
  bool                                              is_hard_rt_prev_;
  
  void getSharedMemoryStruct( SharedMemoryStruct* shmem );
  void setSharedMemoryStruct( const SharedMemoryStruct* shmem);
};



/**
 * 
 * 
 * 
 * 
 * 
 * 
 */
inline 
SharedMemory::SharedMemory(const std::string& identifier, double operational_time, double watchdog_decimation, const ShmAccessMode& mode, const size_t dim )
: access_mode_         ( mode                 )
, operational_time_    ( operational_time     )
, watchdog_            ( watchdog_decimation  )
, name_                ( identifier  )
, dim_data_            ( dim )
, dim_with_header_     ( dim + sizeof(SharedMemory::SharedMemoryStruct::Header) )  //time and bonding index
, start_watchdog_time_ ( -1 )
, time_prev_           ( 0  )
, bond_cnt_            ( 0  )
, bonded_prev_         ( false )
, is_hard_rt_prev_     ( false )
{
  bool ok = true;
  try
  {
    ROS_INFO ( "[%sSTART%s] %sShMem Init%s [ %s%s%s ]", BOLDMAGENTA, RESET, BOLDBLUE, RESET, BOLDCYAN, name_.c_str(), RESET );
    if ( dim_data_ == 0 )
    {
      ROS_WARN ( "[%sCHECK%s%s] %sShMem Init%s [ %s%s%s%s ] Memory does not exist, is it correct?"
                , BOLDRED, RESET, YELLOW, BLUE, RESET, BOLDCYAN, name_.c_str(), RESET, YELLOW );
      return;
    }
    
    //------------------------------------------
    boost::interprocess::permissions permissions ( 0677 );
    if ( access_mode_ == CREATE )
    {
      if ( !boost::interprocess::named_mutex::remove ( name_.c_str() ) )
      {
        ROS_DEBUG ( "[CHECK] %sShMem Init%s [ %s%s%s%s ] Error in Removing Mutex", BLUE, YELLOW, BOLDCYAN, name_.c_str(), RESET, YELLOW );
      }

      if ( !boost::interprocess::shared_memory_object::remove ( name_.c_str() ) )
      {
        ROS_DEBUG ( "[CHECK] %sShMem Init%s [ %s%s%s%s ] Error in Removing Shmem", BLUE, YELLOW, BOLDCYAN, name_.c_str(), RESET, YELLOW );
      }

      // store old
      mode_t old_umask = umask ( 0 );

      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Sizeof uint8 %zu sizeof double %zu sizeof header %zu", BLUE, RESET, BOLDCYAN, name_.c_str(), RESET,  sizeof(uint8_t), sizeof(double), sizeof(SharedMemoryStruct::Header) );
      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Create memory (bytes %s%zu/%zu%s)", BLUE, RESET, BOLDCYAN, name_.c_str(), RESET,  BOLDCYAN, dim_data_, dim_with_header_, RESET );
      shared_memory_ = boost::interprocess::shared_memory_object( boost::interprocess::create_only
                                                                , name_.c_str()
                                                                , boost::interprocess::read_write
                                                                , permissions );

      shared_memory_.truncate ( dim_with_header_ );
      shared_map_ = boost::interprocess::mapped_region ( shared_memory_, boost::interprocess::read_write );

      std::memset ( shared_map_.get_address(), 0, shared_map_.get_size() );

      mutex_.reset ( new  boost::interprocess::named_mutex ( boost::interprocess::create_only, name_.c_str(), permissions ) );

      // restore old
      umask ( old_umask );
      
    }
    else
    {
      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Bond to Shared Memory (bytes %s%zu/%zu%s)", BLUE, RESET, BOLDCYAN, name_.c_str(), RESET, BOLDCYAN, dim_data_, dim_with_header_, RESET );
      shared_memory_ = boost::interprocess::shared_memory_object( boost::interprocess::open_only
                                                                , name_.c_str()
                                                                , boost::interprocess::read_write );

      shared_map_ = boost::interprocess::mapped_region ( shared_memory_, boost::interprocess::read_write );


      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Bond to Mutex", BLUE, RESET, BOLDCYAN, name_.c_str(), RESET );
      mutex_.reset ( new  boost::interprocess::named_mutex ( boost::interprocess::open_only, name_.c_str() ) );

    }
    ROS_INFO ( "[%s DONE%s] %sShMem Init%s [ %s%s%s ]", BOLDGREEN, RESET, BOLDBLUE, RESET, BOLDCYAN, name_.c_str(), RESET );

  }
  catch ( boost::interprocess::interprocess_exception &e )
  {
    ROS_ERROR_STREAM ( "In processing module '" << identifier << "' got the error: " << e.what() ) ;
    ok = false;
  }
  catch ( std::exception& e )
  {
    ROS_ERROR_STREAM ( "In processing module '" << identifier << "' got the error: " << e.what() );
    ok = false;
  }

  if(!ok)
    throw std::runtime_error("Error in creation of the shared memory. Exit.");
}

inline 
SharedMemory::SharedMemory(const std::string& identifier, double operational_time, double watchdog_decimation )
: access_mode_         ( OPEN                )
, operational_time_    ( operational_time    )
, watchdog_            ( watchdog_decimation )
, name_                ( identifier  )
, dim_data_            ( 0 )
, dim_with_header_     ( 0 + sizeof(SharedMemory::SharedMemoryStruct::Header) )  //time and bonding index
, start_watchdog_time_ ( -1 )
, time_prev_           ( 0  )
, bond_cnt_            ( 0  )
, bonded_prev_         ( false )
, is_hard_rt_prev_     ( false )
{
  bool ok = true;
  try
  {
  
    ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Check Memory", BLUE, RESET, BOLDCYAN, name_.c_str(), RESET );
    shared_memory_ = boost::interprocess::shared_memory_object ( boost::interprocess::open_only
                                                                    , name_.c_str()
                                                                    , boost::interprocess::read_write );

    shared_map_ = boost::interprocess::mapped_region ( shared_memory_, boost::interprocess::read_write );

    ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Check Mutex", BLUE, RESET, BOLDCYAN, name_.c_str(), RESET );
    mutex_.reset ( new  boost::interprocess::named_mutex ( boost::interprocess::open_only, name_.c_str() ) );

    dim_with_header_ = shared_map_.get_size();
    dim_data_        = shared_map_.get_size() - sizeof(SharedMemory::SharedMemoryStruct::Header);

    assert ( dim_data_ > 0 );

    ROS_INFO ( "[%sREADY%s] %sShMem Init%s [ %s%s%s ] Ready", BOLDGREEN, RESET, BLUE, RESET, BOLDCYAN, name_.c_str(), RESET );
  }
  catch ( boost::interprocess::interprocess_exception &e )
  {
    if ( e.get_error_code() == boost::interprocess::not_found_error )
    {
      ROS_WARN ( "[ %s%s%s ] Memory does not exist, Check if correct?", BOLDCYAN, name_.c_str(), YELLOW);
      dim_data_ = 0.0;
      return;
    }
    else
    {
      ROS_ERROR_STREAM ( "In processing module '" << identifier << "' [" << name_ << "] got the error: " << e.what() << " error code: " << e.get_error_code() ) ;
      ok = false;
    }
  }
  catch ( std::exception& e )
  {
    ROS_ERROR_STREAM ( "In processing module '" << identifier << "' got the error: " << e.what() );
    ok = false;
  }

  if(!ok)
    throw std::runtime_error("Error in creation of the shared memory. Exit.");
}


inline
SharedMemory::~SharedMemory()
{
  
  if ( dim_data_ > 0 )
  {
    ROS_INFO ( "[ %s%s%s ][ %sShMem Destructor%s ] Shared Mem Destructor", BOLDCYAN, name_.c_str(), RESET , BOLDBLUE, RESET);
    
    if( isBonded() )
      breakBond();
    
    if ( access_mode_ == CREATE )
    {
    
      ROS_INFO ( "[ %s%s%s ][ %sShMem Destructor%s ] Remove Shared Mem ", BOLDCYAN, name_.c_str(), RESET, BOLDBLUE, RESET);

      if ( ! boost::interprocess::shared_memory_object::remove ( name_.c_str() ) )
      {
        ROS_ERROR ( "Error in removing the shared memory object" );
      }

      ROS_INFO ( "[ %s%s%s ][ %sShMem Destructor%s ] Remove Mutex", BOLDCYAN, name_.c_str(), RESET, BOLDBLUE, RESET);

      if ( !boost::interprocess::named_mutex::remove ( name_.c_str() ) )
      {
        ROS_ERROR ( "[ %s%s%s ][ %sShMem Destructor%s ] Error", BOLDCYAN, name_.c_str(), RED, BOLDBLUE, RESET);
      }
    }
  }
}

inline 
void SharedMemory::getSharedMemoryStruct( SharedMemoryStruct* shmem )
{
  assert( shmem );
  assert ( shared_map_.get_size() < sizeof(SharedMemory::SharedMemoryStruct) );
  
  shmem->clear();
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock ( *mutex_ );  // from local buffer to shared memory
  std::memcpy ( shmem, shared_map_.get_address(), shared_map_.get_size() );
  lock.unlock();
  
}

inline 
void SharedMemory::setSharedMemoryStruct( const SharedMemoryStruct* shmem )
{
  assert( shmem );
  assert ( shared_map_.get_size() < sizeof(SharedMemory::SharedMemoryStruct) );
  
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock ( *mutex_ );
  std::memcpy ( shared_map_.get_address(), shmem, shared_map_.get_size()  );
  lock.unlock();
}


inline 
bool SharedMemory::isHardRT ( ) 
{ 
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);
  
  bool is_hard_rt = (shmem.header_.rt_flag_ ==  1);
  if( is_hard_rt_prev_ != is_hard_rt )
  {
    ROS_WARN( "[ %s%s%s ] RT State Changed from '%s%s%s' to '%s%s%s'", BOLDCYAN, name_.c_str(), RESET
            , BOLDCYAN, ( is_hard_rt_prev_ ? "HARD" : "SOFT" ), RESET
            , BOLDCYAN, ( is_hard_rt       ? "HARD" : "SOFT" ), RESET ) ;
      is_hard_rt_prev_ = is_hard_rt;
  }
  return (shmem.header_.rt_flag_ ==  1);
  
}


inline 
bool SharedMemory::setHardRT ( ) 
{ 
  ROS_INFO( "[ %s%s%s ] [%sSTART%s] Set Hard RT", BOLDCYAN, name_.c_str(), RESET, BOLDCYAN, RESET ) ;
    
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);
  
  if( (shmem.header_.rt_flag_ ==  1) )
  {
    ROS_WARN( "[ %s%s%s%s ] Already hard RT!", BOLDCYAN, name_.c_str(), RESET, RED ) ;
  }
  
  shmem.header_.rt_flag_ = 1;
  setSharedMemoryStruct(&shmem);
  
  ROS_INFO( "[ %s%s%s ] [%s DONE%s] Set Hard RT", BOLDCYAN, name_.c_str(), RESET, BOLDGREEN, RESET ) ;
  return true;
}

inline 
bool SharedMemory::setSoftRT ( ) 
{ 
  ROS_INFO( "[ %s%s%s ] [%sSTART%s] Set Soft RT", BOLDCYAN, name_.c_str(), RESET, BOLDCYAN, RESET ) ;
    
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);
  
  if( (shmem.header_.rt_flag_ ==  0) )
  {
    ROS_WARN( "[ %s%s%s%s ] Already soft RT!", BOLDCYAN, name_.c_str(), RESET, RED ) ;
  }
  
  shmem.header_.rt_flag_ = 0;
  setSharedMemoryStruct(&shmem);
  
  ROS_INFO( "[ %s%s%s ] [%s DONE%s] Set soft RT", BOLDCYAN, name_.c_str(), RESET, BOLDGREEN, RESET ) ;
  return true;
}



inline 
bool SharedMemory::isBonded ( ) 
{ 
  
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);
  
  
  bool is_bonded = (shmem.header_.bond_flag_ == 1);
  if( bonded_prev_ != is_bonded )
  {
    ROS_WARN( "[ %s%s%s ] Bonding State Changed from '%s%s%s' to '%s%s%s'", BOLDCYAN, name_.c_str(), RESET
            , BOLDCYAN, ( bonded_prev_  ? "BONDED" : "UNBONDED" ), RESET
            , BOLDCYAN, ( is_bonded     ? "BONDED" : "UNBONDED" ), RESET ) ;
      bonded_prev_ = is_bonded;
  }
  return (shmem.header_.bond_flag_ == 1);
}


inline 
bool SharedMemory::bond ( ) 
{
  ROS_INFO( "[ %s%s%s ] [%sSTART%s] Bonding", BOLDCYAN, name_.c_str(), RESET, BOLDCYAN, RESET ) ;
  
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);
  
  if( (shmem.header_.bond_flag_ ==  1) )
  {
    ROS_ERROR( "[ %s%s%s%s ] Already Bonded! Abort. \n\n****** RESET CMD FOR SAFETTY **** \n", BOLDCYAN, name_.c_str(), RESET, RED ) ;
    return false;
  }
  
  shmem.header_.bond_flag_ = 1;
  setSharedMemoryStruct(&shmem);
  
  ROS_INFO( "[ %s%s%s ] [%sDONE%s] Bonding", BOLDCYAN, name_.c_str(), RESET, BOLDGREEN, RESET ) ;
  return true;
}

inline 
bool SharedMemory::breakBond ( ) 
{ 
  ROS_INFO( "[ %s%s%s ] Break Bond", BOLDCYAN, name_.c_str(), RESET ) ;
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);
  
  shmem.header_.rt_flag_ = 0;
  shmem.header_.bond_flag_ = 0;
  
  setSharedMemoryStruct(&shmem);
  
  return true;
  
}


inline 
SharedMemory::ErrorCode SharedMemory::update ( const uint8_t* ibuffer, double time, const size_t& n_bytes )
{
  if ( dim_data_ != n_bytes )
  {
    ROS_ERROR ( "FATAL ERROR! Shared memory map '%zu' bytes, while the input is of '%zu' bytes", dim_data_, n_bytes );
    return coe_driver::SharedMemory::UNMACTHED_DATA_DIMENSION;
  }
  
  if ( dim_data_ <= 0 )
  {
    return coe_driver::SharedMemory::NONE_ERROR;
  }
  
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);

  if( shmem.header_.bond_flag_ == 1 )
  {
    shmem.header_.time_ = time;
    std::memcpy( shmem.buffer, ibuffer, dim_data_ );
  }
  
  setSharedMemoryStruct(&shmem);

  return coe_driver::SharedMemory::NONE_ERROR;
}



inline 
SharedMemory::ErrorCode SharedMemory::flush ( uint8_t* obuffer, double* time, double* latency_time, const size_t& n_bytes )
{
  
  if( dim_data_ != n_bytes )
  {
    ROS_ERROR ( "FATAL ERROR! Wrong Memory Dimensions." );
    return coe_driver::SharedMemory::UNMACTHED_DATA_DIMENSION;
  }
  
  if ( dim_data_ <= 0 ) 
  {
    return coe_driver::SharedMemory::NONE_ERROR;
  }
      
  SharedMemoryStruct shmem;
  getSharedMemoryStruct(&shmem);

  if( shmem.header_.bond_flag_ == 1 )
  {
    *time = shmem.header_.time_;
    std::memcpy( obuffer, shmem.buffer, dim_data_ );
  }
  else
  {
   // ROS_ERROR_THROTTLE( 2, "[ %s%s%s%s ] SAFETTY CMD (not bonded)", BOLDCYAN, name_.c_str(), RESET, RED) ;
    *time = 0.0;
    std::memset ( obuffer, 0x0, dim_data_ );
  }
  
  
  // check
  *latency_time = ( *time - time_prev_ );
  if( ( shmem.header_.bond_flag_ == 1 )  && ( *time - time_prev_ ) < 1e-5 )
  {
    struct timespec act_time;
    clock_gettime( CLOCK_MONOTONIC, &act_time);
    
    if( start_watchdog_time_ == -1 ) 
    {
      start_watchdog_time_ = itia_rtutils::timer_to_s( &act_time );
      // ROS_WARN( "FLUSH Start watchdog(%f,%f,%f)!", start_watchdog_time_, *time, time_prev_ );
    }
    
    double watchdog_time = itia_rtutils::timer_to_s( &act_time );

    if( ( watchdog_time - start_watchdog_time_ ) > watchdog_ )
    {
      if( shmem.header_.rt_flag_==1)
      {
        ROS_ERROR_THROTTLE( 2, "[ %s%s%s%s ] Watchdog %fms (allowed: %f) ****** RESET CMD FOR SAFETTY ****"
          , BOLDCYAN, name_.c_str(), RESET, RED, ( watchdog_time - start_watchdog_time_ ), watchdog_ ) ;
        if( access_mode_ == CREATE )
        {
          std::memset ( obuffer, 0x0, dim_data_ );
        }
      }
      else
      {
         ROS_WARN_THROTTLE( 2, "[ %s%s%s%s ] Watchdog %fms (allowed: %f) ****** SOFT RT ACTIVE, NO ACTION ATTENTION****"
          , BOLDCYAN, name_.c_str(), RESET, YELLOW, ( watchdog_time - start_watchdog_time_ ), watchdog_ ) ;
       
      }
        
      return coe_driver::SharedMemory::WATCHDOG;
    }
  }
  else 
  {
    start_watchdog_time_ = -1; 
  }

  time_prev_ = *time;

  return coe_driver::SharedMemory::NONE_ERROR;
}





inline std::string SharedMemory::to_string( SharedMemory::ErrorCode err )
{ 
  std::string ret = "na";
  switch(err)
  {
    case NONE_ERROR               : ret = "SHARED MEMORY NONE ERROR";               break;
    case UNMACTHED_DATA_DIMENSION : ret = "SHARED MEMORY UNMATHCED DATA DIMENSION"; break;
    case UNCORRECT_CALL           : ret = "SHARED MEMORY UNCORRECT CALL SEQUENCE";  break;
    case WATCHDOG                 : ret = "SHARED MEMORY WATCHDOG";                 break;
  }
  return ret; 
}

inline 
size_t SharedMemory::getSize( bool prepend_header ) const 
{ 
  return prepend_header ? dim_with_header_ : dim_data_; 
}

}

#endif
