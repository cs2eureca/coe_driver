#ifndef __COE__BASE_PLUGINS_ROS__IMPL__H___
#define __COE__BASE_PLUGINS_ROS__IMPL__H___


#include "coe_hw_base_plugin.h"

namespace coe_driver
{
  
    
inline void toBoostArray( const coe_core::BaseDataObjectEntry* in, boost::array<uint8_t, 8>& data  )
{
  uint8_t* raw_data = new uint8_t[ in->sizeBytes() ];
  std::memset( &raw_data[0], 0x0, in->sizeBytes() ) ;
  std::memcpy( &raw_data[0], in->data(), in->sizeBytes() ) ;
  std::memcpy( &data[0], &raw_data[0], in->sizeBytes() ) ;  
  delete raw_data;
}

inline void fromBoostArray( const boost::array<uint8_t, 8>& data, coe_core::BaseDataObjectEntry* in  )
{
  uint8_t* raw_data = new uint8_t[ in->sizeBytes() ];
  std::memset( &raw_data[0] , 0x0         , in->sizeBytes() ) ;
  std::memcpy( &raw_data[0] , &data[0]    , in->sizeBytes() ) ;
  std::memcpy( in->data()   , &raw_data[0], in->sizeBytes() ) ;
  delete raw_data;
}

inline void CoeHwPlugin::errorDiagnostic( diagnostic_updater::DiagnosticStatusWrapper &stat )
{
  ros::Time   n  = ros::Time::now();
  std::string st = " [" + boost::posix_time::to_simple_string( n.toBoost() ) +"]";

  if( are_errors_active_ )
  {
    int dim = 0;
    
    error_mtx_.lock();  
    auto const errors_active = errors_active_;
    error_mtx_.unlock();
    for( auto const & err_pair : errors_active )
    {
      std::string  key  = err_pair.first;
      std::string  val = err_pair.second;
      stat.add( key, val );
      dim++;
    }
    stat.summary(diagnostic_msgs::DiagnosticStatus::WARN, module_->getIdentifier() + " [ " + std::to_string( dim ) + " items ]" + st);
  }
  else
    stat.summary(diagnostic_msgs::DiagnosticStatus::OK,  module_->getIdentifier() + "[ None error ]" + st );
 
}


inline CoeHwPlugin::CoeHwPlugin ( ) 
  : prxpdo_           (nullptr)
  , prxpdo_previous_  (nullptr)
  , ptxpdo_           (nullptr)
  , ptxpdo_previous_  (nullptr)
  , stop_thread_      ( false )
  , are_errors_active_( false )
  , operational_time_ ( 0.001 )
{ 
}

inline CoeHwPlugin::~CoeHwPlugin ( ) 
{
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() CoeHwPlugin::~CoeHwPlugin() Plugin Destructor!");
  
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Join the error thread");
  if( error_thread_ )
  {
    stop_thread_ = true;
    error_thread_->join();
    error_thread_.reset();
  }
  
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Rx Break Bond!");
  pdo_shared_memory_->rx_pdo_.breakBond();
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Tx Break Bond!");
  pdo_shared_memory_->tx_pdo_.breakBond();
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Shutdown set_sdo service");
  set_sdo_.shutdown();
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Shutdown get_sdo service");
  get_sdo_.shutdown();
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Clean mem rxpdo");
  if( prxpdo_ != nullptr )          delete [] prxpdo_;
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Clean mem txpdo");
  if( ptxpdo_ != nullptr )          delete [] ptxpdo_;
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Clean mem rxpdo swap");
  if( prxpdo_previous_ != nullptr ) delete [] prxpdo_previous_;
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() Clean mem txpdo swap");
  if( ptxpdo_previous_ != nullptr ) delete [] ptxpdo_previous_;
  ROS_DEBUG("CoeHwPlugin::~CoeHwPlugin() OK");
}
  
  
inline bool CoeHwPlugin::initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, const int address)
{
  ROS_DEBUG("CoeHwPlugin::initiali() Start!");
  
  XmlRpc::XmlRpcValue config;
  if( !nh.getParam(device_coe_parameter, config ) )
  {
    ROS_ERROR("Error in extracting data from '%s/%s'", nh.getNamespace().c_str(), device_coe_parameter.c_str() );
    return false;
  }
  if( !nh.getParam(device_coe_parameter+"/module_list", config ) )
  {
    ROS_ERROR("Error in extracting data from '%s/%s'", nh.getNamespace().c_str(), device_coe_parameter.c_str() );
    return false;
  }
  
  //---
  
  bool ok = false;
  std::string label ="na";
  for( int i=0; i<config.size(); i++ )
  {
    XmlRpc::XmlRpcValue module = config[i];
    int addr = itia::rutils::toInt   ( module,  coe_driver::XmlRpcNetwork::KeysId[ coe_driver::XmlRpcNetwork::ADDRESS] );
    label    = itia::rutils::toString( module,  coe_driver::XmlRpcNetwork::KeysId[ coe_driver::XmlRpcNetwork::LABEL  ] );
    if( addr ==  address )
    {
      ok = true;
    }    
  }
  if(!ok) 
  {
    ROS_ERROR("Error in extracting data from '%s/%s'. The address '%d'", nh.getNamespace().c_str(), device_coe_parameter.c_str() , address);
    return false;
  }
    
  std::string msg = std::string(RESET) 
              + std::string("[ " + ( BOLDCYAN   + label + RESET ) + " ] " )
              + std::string("[ " + ( BOLDBLUE   + std::string( "Init Params" ) + RESET ) + " ] " )
              + std::string("[ " + ( BOLDYELLOW + std::string( " PARAMS" ) + RESET ) + " ] " );
  
  module_.reset( new coe_driver::ModuleDescriptor( nh, device_coe_parameter, label, address, false ) );
  
  if( !nh.getParam(device_coe_parameter+"/operational_time", operational_time_) )
  {
    ROS_ERROR("Error in extracting data from '%s/%s/operational_time'", nh.getNamespace().c_str(), device_coe_parameter.c_str() );
    return false;
  }
  
  ROS_INFO_STREAM(msg << BOLDYELLOW << "Init Node - Configuration from ROS param server" << RESET );
  if( !module_->initNodeConfigurationFromParams ( ) )
  {
    ROS_ERROR_STREAM( msg << " [ " << RED << " ERROR " << RESET << " ] Basic Configurations from Param Server failed. ");
    return false;
  }
  if( !module_->initNodeCoeConfigurationFromParams ( false ) )
  {
    ROS_ERROR_STREAM( msg << " [ " << RED << " ERROR " << RESET << " ] Coe Configurations from Param Server failed. ");
    return false;
  }
  
  ROS_INFO_STREAM(msg << BOLDYELLOW << "Init Handles" << RESET );
  if( !module_->initHandles( ) )
  {
    ROS_ERROR_STREAM( msg << " [ " << RED << " ERROR " << RESET << " ] Handles Configurations from Params failed. ");
    return false;
  }
  
  if( !module_->connectHandles( ) )
  {
    ROS_ERROR_STREAM( msg << " [ " << RED << " ERROR " << RESET << " ] Handle Connection failed. ");
    return false;
  }
  // TODO check correctness throu network_
  
  set_sdo_  = nh.serviceClient<coe_driver::SetSdo>(device_coe_parameter+"/"+SET_SDO_SERVER_NAMESPACE);
  if (!set_sdo_.waitForExistence(ros::Duration(5)))
  {
    ROS_ERROR("No server found for service %s",set_sdo_.getService().c_str());
    return false;
  }
  get_sdo_  = nh.serviceClient<coe_driver::GetSdo>(device_coe_parameter+"/"+GET_SDO_SERVER_NAMESPACE);
  if (!get_sdo_.waitForExistence(ros::Duration(5)))
  {
    ROS_ERROR("No server found for service %s",get_sdo_.getService().c_str());
    return false;
  }
  ROS_INFO_STREAM(msg << BOLDYELLOW << "Access to Shared Memory '"<<  BOLDCYAN << module_->getIdentifier() << BOLDYELLOW<< "' "<< RESET << " Operational Time: " << operational_time_ << " WatchDog Deciamtion: " << module_->getWatchdogDecimation() );
  pdo_shared_memory_.reset( new coe_driver::ModuleSharedMemory( module_->getIdentifier(), operational_time_, module_->getWatchdogDecimation() ) );
  
  size_t rxpdo_shdim = pdo_shared_memory_->rx_pdo_.getSize(false);
  size_t txpdo_shdim = pdo_shared_memory_->tx_pdo_.getSize(false);
  
  ROS_INFO_STREAM(msg << "Allocate the shared memory object for '" << module_->getIdentifier() << "' [I:" << txpdo_shdim <<"B O:"<< rxpdo_shdim  <<"B]");
  
  prxpdo_          = rxpdo_shdim > 0  ? ( new uint8_t[ rxpdo_shdim ] ) : NULL;
  prxpdo_previous_ = rxpdo_shdim > 0  ? ( new uint8_t[ rxpdo_shdim ] ) : NULL;
  
  ptxpdo_          = txpdo_shdim > 0  ? ( new uint8_t[ txpdo_shdim ] ) : NULL;
  ptxpdo_previous_ = txpdo_shdim > 0  ? ( new uint8_t[ txpdo_shdim ] ) : NULL;
  
  if( prxpdo_           != NULL ) std::memset( prxpdo_, 0x0, rxpdo_shdim );
  if( prxpdo_previous_  != NULL ) std::memset( prxpdo_previous_, 0x0, rxpdo_shdim );
  
  if( ptxpdo_           != NULL ) std::memset( ptxpdo_, 0x0, txpdo_shdim );
  if( ptxpdo_previous_  != NULL ) std::memset( ptxpdo_previous_, 0x0, txpdo_shdim );
    
  if( !pdo_shared_memory_->rx_pdo_.bond() )
  {
    ROS_ERROR ("Bonding failed ...");
    return false;
  }
  
  if( !pdo_shared_memory_->tx_pdo_.bond() )
  {
    ROS_ERROR ("Bonding failed ...");
    return false;
  }
  
  updater_.setHardwareID(module_->getIdentifier());
  updater_.add( module_->getIdentifier(), this, &CoeHwPlugin::errorDiagnostic);
  
  boost::thread::attributes err_thread_attr;
  error_thread_.reset( new boost::thread( err_thread_attr, boost::bind( &CoeHwPlugin::errorThread, this) ) );
  
  
  ROS_DEBUG("CoeHwPlugin::initiali() Ok!");
  return true;
}

inline bool CoeHwPlugin::setHardRT ( )
{
  return pdo_shared_memory_->rx_pdo_.setHardRT() && pdo_shared_memory_->tx_pdo_.setHardRT(); 
}

inline bool CoeHwPlugin::setSoftRT ( )
{
  return pdo_shared_memory_->rx_pdo_.setSoftRT() && pdo_shared_memory_->tx_pdo_.setSoftRT(); 
}

inline bool CoeHwPlugin::read() 
{
  
  if( pdo_shared_memory_->tx_pdo_.getSize(false) == 0 )
    return true;

  try
  {
    assert( pdo_shared_memory_->tx_pdo_.getSize( false ) > 0 );
    if( pdo_shared_memory_->tx_pdo_.getSize(false) !=  module_->sizeInputs() )
    {
      ROS_ERROR("Dimension Mismatch. Module Name '%s', Shared Memory Dimension: %zuB, Module Inputs size %zuB", module_->getIdentifier().c_str(), pdo_shared_memory_->tx_pdo_.getSize(false), module_->sizeInputs() );
      assert(0);
    }
    assert( ptxpdo_previous_ != NULL );
    
    std::memcpy( ptxpdo_previous_, ptxpdo_, pdo_shared_memory_->tx_pdo_.getSize( false ) ); 
    
    coe_driver::PdoSharedMemory::ErrorCode errorcode;
    double tm;
    double latency;
    errorcode = pdo_shared_memory_->tx_pdo_.flush( &ptxpdo_[0], &tm, &latency, module_->sizeInputs() );
    if( errorcode )
    {
      ROS_ERROR("Broken linkage to the Shared Memory. Error: %s Abort.", pdo_shared_memory_->tx_pdo_.to_string( errorcode ).c_str() );
      return false;
    }
    
    module_->updateInputs(&ptxpdo_[0], false);

  }
  catch( std::exception& e )
  {
    ROS_ERROR("READ EXCEPTION: %s",e.what());
    return false;
  }
  return true;
}

  

inline bool CoeHwPlugin::write() 
{
  struct timespec update_time;
 
  try
  {
    if( pdo_shared_memory_->rx_pdo_.getSize(false) == 0 )
      return true;
    
    assert( pdo_shared_memory_->rx_pdo_.getSize(false) ==  module_->sizeOutputs() );
    assert( prxpdo_previous_ != NULL );

    std::memcpy( prxpdo_previous_, prxpdo_, pdo_shared_memory_->rx_pdo_.getSize( false ) );   
    
    module_->getRxPdo().flush( prxpdo_, false );

    clock_gettime(CLOCK_MONOTONIC, &update_time );
    double act_time = itia_rtutils::timer_to_s( &update_time ); 
    coe_driver::PdoSharedMemory::ErrorCode errorcode;
    errorcode=pdo_shared_memory_->rx_pdo_.update( &prxpdo_[0], act_time, module_->sizeOutputs()  );
    if(errorcode)
    {
      ROS_ERROR("Broken linkage to the Shared Memory. Error: %s Abort.", pdo_shared_memory_->rx_pdo_.to_string( errorcode ).c_str() );
      return false;
    }
  }
  catch( std::exception& e )
  {
    ROS_ERROR("WRITE EXCEPTION: %s",e.what());
    return false;
  }
  return true;
}
  
inline bool CoeHwPlugin::readSdo ( coe_core::BaseDataObjectEntry* in)
{
  // ROS_INFO("SDO READ REQUEST: %s", in->to_string().c_str() );
  coe_driver::GetSdo::Request req;
  coe_driver::GetSdo::Response res;
  
  req.index     = in->index();
  req.subindex  = in->subindex();
  req.sdotype   = in->type() == ECT_UNSIGNED8  ? coe_driver::SetSdo::Request::TYPE_U8
                : in->type() == ECT_UNSIGNED16 ? coe_driver::SetSdo::Request::TYPE_U16
                : in->type() == ECT_UNSIGNED32 ? coe_driver::SetSdo::Request::TYPE_U32
                : in->type() == ECT_UNSIGNED64 ? coe_driver::SetSdo::Request::TYPE_U64
                : in->type() == ECT_INTEGER8   ? coe_driver::SetSdo::Request::TYPE_I8
                : in->type() == ECT_INTEGER16  ? coe_driver::SetSdo::Request::TYPE_I16
                : in->type() == ECT_INTEGER32  ? coe_driver::SetSdo::Request::TYPE_I32
                : in->type() == ECT_INTEGER64  ? coe_driver::SetSdo::Request::TYPE_I64
                : 99;

  assert( req.sdotype != 99 );
  
  req.desc      = in->name();
  req.module_id = module_->getIdentifier();
  req.timeout   = 0.1;
  
  if( !set_sdo_.exists() )
  {
    ROS_FATAL("Error. The service '%s' is not available", set_sdo_.getService().c_str() );
    return false;
  }
  
  if( !get_sdo_.call(req,res) )
  {
    ROS_FATAL("Error. Server does not answer to the call. ");
    return false;
  }
  if( !res.success )
  {
    ROS_FATAL("Error. Getting the sdo '%x:%u' failed. ", in->index(), in->subindex() );
    return false;
  }
  // ROS_INFO("Read SDO Success, %lu" , *(uint64_t*)&(res.value[0]) );
  fromBoostArray( res.value, in);
  return true;
}
    
inline bool CoeHwPlugin::writeSdo ( const coe_core::BaseDataObjectEntry* in) 
{
  // ROS_INFO_STREAM( "[" << BOLDMAGENTA << " PROCESS "<< RESET << "] Set COB-ID: " << in->to_string() );
  coe_driver::SetSdo::Request req;
  coe_driver::SetSdo::Response res;
  
  req.index     = in->index();
  req.subindex  = in->subindex();

  req.sdotype   = in->type() == ECT_UNSIGNED8  ? coe_driver::SetSdo::Request::TYPE_U8
                : in->type() == ECT_UNSIGNED16 ? coe_driver::SetSdo::Request::TYPE_U16
                : in->type() == ECT_UNSIGNED32 ? coe_driver::SetSdo::Request::TYPE_U32
                : in->type() == ECT_UNSIGNED64 ? coe_driver::SetSdo::Request::TYPE_U64
                : in->type() == ECT_INTEGER8   ? coe_driver::SetSdo::Request::TYPE_I8
                : in->type() == ECT_INTEGER16  ? coe_driver::SetSdo::Request::TYPE_I16
                : in->type() == ECT_INTEGER32  ? coe_driver::SetSdo::Request::TYPE_I32
                : in->type() == ECT_INTEGER64  ? coe_driver::SetSdo::Request::TYPE_I64
                : 99;

  assert( req.sdotype != 99 );
  
  toBoostArray(in, req.value) ;     
  
  req.desc      = in->name();
  
  req.module_id = module_->getIdentifier();
  
  req.timeout   = 0.1;
  
  if( !set_sdo_.exists() )
  {
    ROS_FATAL("Error. The service '%s' is not available", set_sdo_.getService().c_str() );
    return false;
  }
  
  if( !set_sdo_.call(req,res) )
  {
    ROS_FATAL("Error. Server does not asnwer to the call. ");
    return false;
  }
  if( !res.success )
  {
    ROS_INFO_STREAM( "[" << BOLDRED << "  FAILED "<< RESET << "] Set COB-ID: " << in->to_string() );
    return false;
  }
  // ROS_INFO_STREAM( "[" << BOLDGREEN << "   OK    "<< RESET << "] Set COB-ID: " << in->to_string() );
  return true;
}

inline void  CoeHwPlugin::errorThread( )
{
  ros::Rate rt(1);
  while( ros::ok() && !stop_thread_ )
  {
    if( are_errors_active_ )
    {
      std::lock_guard<std::mutex> lock(error_mtx_);  
      getErrors( errors_active_ );
    }
    updater_.update();
    rt.sleep();
  }
}

}

#endif