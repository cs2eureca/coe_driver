#ifndef __COE__BASE_PLUGINS_ROS__H___
#define __COE__BASE_PLUGINS_ROS__H___

#include <functional>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <diagnostic_updater/diagnostic_updater.h>
#include <itia_futils/itia_futils.h>
#include <itia_rutils/itia_rutils.h>
#include <itia_rtutils/itia_rtutils.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_driver/GetSdo.h>
#include <coe_driver/SetSdo.h>
#include <coe_driver/modules/coe_pdo_shared_memory.h>
#include <coe_driver/modules/coe_module_descriptor_xmlrpc.h>
#include <coe_driver/modules/coe_network_descriptor_xmlrpc.h>

#include <coe_driver/diagnostic/coe_generic_analyzer.h>

static const char* SET_SDO_SERVER_NAMESPACE = "set_sdo";
static const char* GET_SDO_SERVER_NAMESPACE = "get_sdo";

namespace coe_driver
{
  
  class CoeHwPlugin
  {
  private:
    
    // Local pointer to easy manage the access to the shared memory
    uint8_t*                            prxpdo_;
    uint8_t*                            prxpdo_previous_;
    
    uint8_t*                            ptxpdo_;
    uint8_t*                            ptxpdo_previous_;
    
    // Structure to manage the protected access to the shared memory
    coe_driver::ModuleSharedMemoryPtr   pdo_shared_memory_;
    
    // Methods to access the node through sdo
    ros::ServiceClient                  set_sdo_;
    ros::ServiceClient                  get_sdo_;
    
    //
    bool                                stop_thread_;
    
    // Error management
    std::mutex                                        error_mtx_;
    std::vector< std::pair<std::string,std::string> > errors_active_;
    bool                                              are_errors_active_;
    boost::shared_ptr<boost::thread>                  error_thread_;
    diagnostic_updater::Updater                       updater_;
    void errorThread( );
    void errorDiagnostic( diagnostic_updater::DiagnosticStatusWrapper &stat );

  protected:
    
    double                          operational_time_;
    coe_driver::ModuleDescriptorPtr module_;
    
    virtual void getErrors ( std::vector< std::pair< std::string,std::string> >& errors_map ) = 0;   
    virtual bool checkErrorsActive ( ) = 0;   
    
    
  public:
    
    CoeHwPlugin ( );
    virtual ~CoeHwPlugin ( );
    
    virtual bool initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address );
    virtual bool setHardRT ( );
    virtual bool setSoftRT ( );
    virtual bool read  ( );    
    virtual bool write ( );    
    
    virtual bool readSdo  ( coe_core::BaseDataObjectEntry* in);
    virtual bool writeSdo ( const coe_core::BaseDataObjectEntry* in);

    std::string  getUniqueId       ( ) const  { return module_->getIdentifier(); }
    bool         areErrorsActive   ( ) const  { return are_errors_active_; }   
    
    virtual std::vector<std::string> getStateNames     ( ) const                         = 0;
    virtual std::string              getActualState    ( )                               = 0;
    virtual bool                     setTargetState    ( const std::string& state_name ) = 0;    
    
    virtual std::vector<std::string> getAnalogInputNames  ( ) const = 0;
    virtual std::vector<std::string> getAnalogOutputNames ( ) const = 0;;
    virtual std::vector<std::string> getDigitalInputNames ( ) const = 0;
    virtual std::vector<std::string> getDigitalOutputNames( ) const = 0;
    
    virtual void    jointStateHandle        ( double** pos,  double** vel, double** eff ) = 0;
    virtual void    jointCommandHandle      ( double** pos,  double** vel, double** eff )  = 0;
    
    virtual double* analogInputValueHandle  ( const std::string& name ) = 0;
    virtual double* analogOutputValueHandle ( const std::string& name ) = 0;

    virtual bool*   digitalInputValueHandle ( const std::string& name ) = 0;
    virtual bool*   digitalOutputValueHandle( const std::string& name ) = 0;
    
    virtual bool hasDigitalInputs()=0;
    virtual bool hasDigitalOutputs()=0;
    virtual bool hasAnalogInputs()=0;
    virtual bool hasAnalogOutputs()=0;
    virtual bool isActuator()=0;
    
  };
  
    
}



#include "coe_hw_base_plugin_impl.hpp"
    
    
    
#endif