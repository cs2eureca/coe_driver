
#ifndef __coe__ros__network_decriptor__xmlrpc__h__
#define __coe__ros__network_decriptor__xmlrpc__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <tuple>
#include <ros/ros.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_driver/modules/coe_module_descriptor.h>
#include <coe_driver/modules/coe_module_descriptor_xmlrpc.h>
#include <coe_driver/modules/coe_network_descriptor.h>

namespace coe_driver 
{

  
namespace XmlRpcNetwork
{

  static const char*  KeysId[9] = { "label", "address", "mapped", "default" };
  enum                KeysCode    { LABEL=0, ADDRESS, MAPPED, DEFAULT };

  typedef std::tuple < std::string, uint16_t, bool, bool >XmlRpcModuleList;

  inline bool toXmlRpcValue( const XmlRpcModuleList& t, XmlRpc::XmlRpcValue& xml_value  )
  {
    xml_value[ KeysId[ KeysCode::LABEL    ] ] = std::get< KeysCode::LABEL   >  ( t );
    xml_value[ KeysId[ KeysCode::ADDRESS  ] ] = std::get< KeysCode::ADDRESS >  ( t );
    xml_value[ KeysId[ KeysCode::MAPPED   ] ] = std::get< KeysCode::MAPPED  >  ( t );
    xml_value[ KeysId[ KeysCode::DEFAULT  ] ] = std::get< KeysCode::DEFAULT >  ( t );
    return true;
  }
  
  template<typename T>
  void setParam(ros::NodeHandle& nh, const std::string& key, const std::vector< T >& vec)
  {
  
    // Note: the XmlRpcValue starts off as "invalid" and assertArray turns it
    // into an array type with the given size
    XmlRpc::XmlRpcValue xml_vec;
    xml_vec.setSize(vec.size());

    // Copy the contents into the XmlRpcValue
    for(size_t i=0; i < vec.size(); i++) 
    {
      toXmlRpcValue( vec.at(i), xml_vec[i] );  
    }
    ros::param::set(nh.resolveName(key), xml_vec);
  }


}



}

#endif
