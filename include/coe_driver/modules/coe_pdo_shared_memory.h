
#ifndef __coe__ros__pdo__shared_memeory__h__
#define __coe__ros__pdo__shared_memeory__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>

#include <tuple>
#include <ros/ros.h>
#include <bondcpp/bond.h>

#include <coe_core/coe_pdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_driver/modules/coe_network_descriptor.h>

#include <coe_driver/common/coe_shared_memory.h>

namespace coe_driver 
{

inline size_t pdoSharedMemoryDim( const ModuleDescriptorPtr& module, const int sdo_assignement )
{
  size_t dim = (sdo_assignement == ECT_SDO_RXPDOASSIGN ? module->getRxPdo().nBytes ( true ) : module->getTxPdo().nBytes ( true ) );
  return dim;
}

inline std::string pdoSharedMemoryIdentifier( const ModuleDescriptorPtr& module, const int sdo_assignement )
{
  return ( sdo_assignement == ECT_SDO_RXPDOASSIGN ? module->getSoemOutputId() : module->getSoemInputId() );
}
inline std::string pdoSharedMemoryIdentifier( const std::string& identifier, const int sdo_assignement )
{
  return (identifier + ( sdo_assignement == ECT_SDO_RXPDOASSIGN ? "_rxpdo" : "_txpdo" ));
  
}





class PdoSharedMemory : public SharedMemory
{
public:
  typedef std::shared_ptr< PdoSharedMemory >  Ptr;
  
  PdoSharedMemory(const ModuleDescriptorPtr& module, const int sdo_assignement, double operational_time, const ShmAccessMode& mode )
  : SharedMemory( pdoSharedMemoryIdentifier(module, sdo_assignement)
                , operational_time
                , (double) module->getWatchdogDecimation() * operational_time
                , mode
                , pdoSharedMemoryDim( module, sdo_assignement ) )
  , sdo_assignement_ ( sdo_assignement )
  {
  }

  PdoSharedMemory(const std::string& identifier, const int sdo_assignement, double operational_time, int watchdog_decimation )
  : SharedMemory( pdoSharedMemoryIdentifier(identifier, sdo_assignement )
                , operational_time
                , (double)watchdog_decimation * operational_time )
  , sdo_assignement_ ( sdo_assignement )
  {
    // nothing to do so far
  }
  
  ErrorCode update   ( const uint8_t* buffer, const double time, const size_t& n_bytes );
  ErrorCode flush    ( uint8_t* buffer, double* time, double* latency_time, const size_t& n_bytes );
  
private:
  const int sdo_assignement_ ;
  
  
};

typedef PdoSharedMemory::Ptr PdoSharedMemoryPtr;




class ModuleSharedMemory
{
public:
  typedef std::shared_ptr< ModuleSharedMemory > Ptr;
  
  const std::string   identifier_;
  PdoSharedMemory     rx_pdo_;
  PdoSharedMemory     tx_pdo_;
  
  ModuleSharedMemory( const ModuleDescriptorPtr& module, double operational_time, const PdoSharedMemory::ShmAccessMode& mode );
  ModuleSharedMemory( const std::string& identifier, double operational_time, int watchdog_decimation);
  ~ModuleSharedMemory();
};

typedef ModuleSharedMemory::Ptr ModuleSharedMemoryPtr;


class ModulesSharedMemory
{
  
public:
  
  typedef std::vector<ModuleSharedMemoryPtr>   List;
  typedef List::iterator                       iterator;
  typedef List::const_iterator                 const_iterator;
  
  ~ModulesSharedMemory( );
  
  
  void                          clear()       ;
  iterator                      begin()       ;
  iterator                      end()         ;
                                              
  const_iterator                begin()  const;
  const_iterator                end()    const;
                                              
  const_iterator                cbegin() const;
  const_iterator                cend()   const;
    
  const ModuleSharedMemoryPtr&  operator[]( const std::string& i ) const;
  ModuleSharedMemoryPtr& operator[]( const std::string& i );
  
  bool insert( ModuleSharedMemoryPtr module_shm ) ;
  
private:
  
  List modules_shm_;
  
};



}

#endif
