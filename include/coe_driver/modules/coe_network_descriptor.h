
#ifndef __coe__ros__network_decriptor__h__
#define __coe__ros__network_decriptor__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <tuple>
#include <ros/ros.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_driver/modules/coe_module_descriptor.h>

namespace coe_driver 
{

  
class NetworkDescriptor
{
private:
  
  ros::NodeHandle                             nh_;
  std::string                                 param_root_;
  
  std::string                                 adapter_;
  double                                      operational_time_;
  std::map< int , std::string>                module_addresses_map_;
  std::map< int, bool >                       module_default_map_;
  
public:
  
  typedef std::shared_ptr< NetworkDescriptor > Ptr;

  NetworkDescriptor(   )
  {
    // nothing to do so far
  }

  NetworkDescriptor( ros::NodeHandle& nh, const std::string& coe_device_parameter_namespace  )
  : nh_( nh ), param_root_(coe_device_parameter_namespace), adapter_("none"), operational_time_(-1)
  {
    // nothing to do so far
  }

    
  void initialize( ros::NodeHandle& nh, const std::string& coe_device_parameter_namespace  )
  {
    nh_ =  nh; 
    param_root_ = coe_device_parameter_namespace;
    adapter_ = "none";
    operational_time_ = -1;
  }
  
  
  bool initNetworkNames ( );
  
  const std::string&         getNamespace           ( )    const { return param_root_; }
  const std::string&         getAdapterName         ( )    const { return adapter_; }
  const double&              getOperationalTime     ( )    const { return operational_time_; }
                                                    
  std::vector<int>           getAddresses           ( )    const;
  std::vector<std::string>   getNodeUniqueIDs       ( )    const;
  std::map<int,std::string>  getAddressLabelsMap    ( )    const;
  std::map<int,std::string>  getAddressUniqueIdMap  ( )    const;
  
  bool                       hasDefaultConfiguration( int addr ) const;
  
  bool                       checkAddress           ( int addr )                const;
  int                        checkModuleName        ( const std::string& id, const bool verbose = false ) const;
       

};


typedef NetworkDescriptor::Ptr NetworkDescriptorPtr;

}

#endif
