
#ifndef __coe__ros__module__descriptor__xmlrpc__h__
#define __coe__ros__module__descriptor__xmlrpc__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <tuple>
#include <ros/ros.h>
#include <coe_core/coe_pdo.h>
#include <itia_rutils/itia_rutils_xmlrpc.h>
#include <coe_core/coe_pdo_xmlrpc.h>
#include <coe_core/coe_sdo_xmlrpc.h>
#include <coe_driver/modules/coe_module_descriptor.h>

namespace coe_driver
{
  

namespace XmlRpcAxisData
{

  static const char* KeysId[4]= { "name"
                                , "scale"
                                , "offset"
                                , "pdo_subindex" };
                                
  enum KeysCode                 { NAME = 0
                                , SCALE
                                , OFFSET
                                , PDO_IDX };
                                
  
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, std::map<std::string,coe_driver::ModuleDescriptor::AxisDataEntry>& entries, const std::string& log )
  {
    
    XmlRpc::XmlRpcValue config( node );
    if ( config.getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_ERROR ( "The node  is not of type array. %d/%d", int( config.getType()), int( XmlRpc::XmlRpcValue::TypeArray ) );
      throw std::runtime_error("The node  is not of type array");
    }
    
    entries.clear();
    for(size_t i=0; i < size_t( config.size() ); i++) 
    {
      coe_driver::ModuleDescriptor::AxisDataEntry entry;
      std::string name    = itia::rutils::toString( config[i], KeysId[ NAME    ] , log + ", " + std::to_string(i)+"# name");
      entry.scale         = itia::rutils::toDouble( config[i], KeysId[ SCALE   ] , log + ", " + std::to_string(i)+"# scale");
      entry.offset        = itia::rutils::toDouble( config[i], KeysId[ OFFSET  ] , log + ", " + std::to_string(i)+"# offset");
      entry.pdo_subindex  = itia::rutils::toInt   ( config[i], KeysId[ PDO_IDX ] , log + ", " + std::to_string(i)+"# pdo subindex");
      entries.insert(std::make_pair(name, entry));
    }
  }
    
  inline void toXmlRpcValue( const std::map<std::string,coe_driver::ModuleDescriptor::AxisDataEntry>& entries, XmlRpc::XmlRpcValue& xml_value  )
  {
    const size_t  nentries = entries.size();
    xml_value.setSize( nentries );
    size_t i = 0;
    for( auto const & entry : entries )
    {
      itia::rutils::toXmlRpcValue((std::string)entry.first                , xml_value[ i ][ KeysId[ NAME    ] ] );
      itia::rutils::toXmlRpcValue((double)     entry.second.scale         , xml_value[ i ][ KeysId[ SCALE   ] ]  );
      itia::rutils::toXmlRpcValue((double)     entry.second.offset        , xml_value[ i ][ KeysId[ OFFSET  ] ]  );
      itia::rutils::toXmlRpcValue((int)        entry.second.pdo_subindex  , xml_value[ i ][ KeysId[ PDO_IDX ] ]  );
      i++;
    }
  }

};

namespace XmlRpcAnalogData
{
/* EQUAL TO XmlRpcAxisData
  static const char* KeysId[5]= { "name"
                                , "scale"
                                , "offset"
                                , "pdo_subindex" };
                                
  enum KeysCode                 { NAME = 0
                                , SCALE
                                , OFFSET
                                , PDO_IDX };
*/                              
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, std::map<std::string,coe_driver::ModuleDescriptor::AnalogDataEntry>& entries, const std::string& log )
  {
    std::map<std::string,coe_driver::ModuleDescriptor::AxisDataEntry>  entries_;
    coe_driver::XmlRpcAxisData::fromXmlRpcValue( node, entries_, log );
    
    entries.clear();
    for( auto const entry : entries_ )
      entries.insert( std::make_pair( entry.first, *(coe_driver::ModuleDescriptor::AnalogDataEntry*)&(entry.second) ) );
    
  }
  
  inline void toXmlRpcValue( const std::map<std::string,coe_driver::ModuleDescriptor::AnalogDataEntry>& entries, XmlRpc::XmlRpcValue& xml_value  )
  {
    std::map<std::string,coe_driver::ModuleDescriptor::AxisDataEntry>  entries_;
    for( auto const entry : entries )
      entries_.insert( std::make_pair( entry.first, *(coe_driver::ModuleDescriptor::AxisDataEntry*)&(entry.second) ) );
    
    return coe_driver::XmlRpcAxisData::toXmlRpcValue( entries_, xml_value );
  }
                                
};

namespace XmlRpcDigitalData
{

  static const char* KeysId[3]= { "name"
                                , "pdo_subindex"
                                , "bit" };
                                
  enum KeysCode                 { NAME = 0
                                , PDO_IDX 
                                , BIT };
                                
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, std::map<std::string,coe_driver::ModuleDescriptor::DigitalDataEntry>& entries, const std::string& log )
  {
    
    XmlRpc::XmlRpcValue config( node );
    if ( config.getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_ERROR ( "The node  is not of type array. %d/%d", int( config.getType()), int( XmlRpc::XmlRpcValue::TypeArray ) );
      throw std::runtime_error("The node  is not of type array");
    }
    
    entries.clear();
    for(size_t i=0; i < size_t( config.size() ); i++) 
    {
      coe_driver::ModuleDescriptor::DigitalDataEntry entry;
      std::string name    = itia::rutils::toString( config[i], KeysId[ NAME    ] , log + ", " + std::to_string(i)+"# name");
      entry.pdo_subindex  = itia::rutils::toInt   ( config[i], KeysId[ PDO_IDX ] , log + ", " + std::to_string(i)+"# pdo subindex");
      entry.bit           = itia::rutils::toInt   ( config[i], KeysId[ BIT     ] , log + ", " + std::to_string(i)+"# bit");
      
      entries.insert(std::make_pair( name, entry) );
    }
  }
    
  inline void toXmlRpcValue( const std::map<std::string,coe_driver::ModuleDescriptor::DigitalDataEntry>& entries, XmlRpc::XmlRpcValue& xml_value  )
  {
    const size_t  nentries = entries.size();
    xml_value.setSize( nentries );
    size_t i = 0;
    for( auto const & entry : entries )
    {  
      itia::rutils::toXmlRpcValue((std::string)entry.first              , xml_value[ i ][ KeysId[ NAME    ] ]  );
      itia::rutils::toXmlRpcValue((int)        entry.second.pdo_subindex, xml_value[ i ][ KeysId[ PDO_IDX ] ]  );
      itia::rutils::toXmlRpcValue((int)        entry.second.bit         , xml_value[ i ][ KeysId[ BIT ]     ]  );
      i++;
    }
  }

};                                              

namespace XmlRpcWordData
{

  static const char* KeysId[5]= { "name"
                                , "pdo_subindex" };
                                
  enum KeysCode                 { NAME = 0
                                , PDO_IDX };
                              
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, std::map<std::string,coe_driver::ModuleDescriptor::WordDataEntry>& entries, const std::string& log )
  {
    
    XmlRpc::XmlRpcValue config( node );
    if ( config.getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_ERROR ( "The node  is not of type array. %d/%d", int( config.getType()), int( XmlRpc::XmlRpcValue::TypeArray ) );
      throw std::runtime_error("The node  is not of type array");
    }
    
    entries.clear();
    for(size_t i=0; i < size_t( config.size() ); i++) 
    {
      coe_driver::ModuleDescriptor::WordDataEntry entry;
      std::string name    = itia::rutils::toString( config[i], KeysId[ NAME    ] , log + ", " + std::to_string(i)+"# name");
      entry.pdo_subindex  = itia::rutils::toInt   ( config[i], KeysId[ PDO_IDX ] , log + ", " + std::to_string(i)+"# pdo subindex");
      entries.insert(std::make_pair(name, entry));
    }
  }
    
  inline void toXmlRpcValue( const std::map<std::string,coe_driver::ModuleDescriptor::WordDataEntry>& entries, XmlRpc::XmlRpcValue& xml_value  )
  {
    const size_t  nentries = entries.size();
    xml_value.setSize( nentries );
    size_t i = 0;
    for( auto const & entry : entries )
    {
      itia::rutils::toXmlRpcValue((std::string)entry.first                , xml_value[ i ][ KeysId[ NAME    ] ] );
      itia::rutils::toXmlRpcValue((int)        entry.second.pdo_subindex  , xml_value[ i ][ KeysId[ PDO_IDX ] ]  );
      i++;
    }
  }
                                
};


namespace XmlRpcModule
{

  static const char*  KeysId[] =  { "model"                           // # 1
                                  , "description"                     // # 2
                                  , "enable_dc"                      // # 3
                                  , "sdo_complete_access"             // # 4
                                  , "loop_rate_decimation"
                                  , "watchdog_decimation"
                                  , "axis_feedback"
                                  , "axis_command"
                                  , "analog_inputs"
                                  , "analog_outputs"
                                  , "digital_inputs"
                                  , "digital_outputs"
                                  , "word_inputs"
                                  , "word_outputs"
                                  , "rxpdo"
                                  , "rxpdo_packed_size"
                                  , "txpdo"
                                  , "txpdo_packed_size"
                                  , "sdo" 
                                  };
  enum                KeysCode    { MODEL = 0
                                  , DESCRIPTION
                                  , ENABLE_DC 
                                  , SUPPORT_SDO_CA
                                  , LOOP_RATE_DECIMATION
                                  , WATCHDOG_DECIMATION
                                  , AXIS_FEEDBACK
                                  , AXIS_COMMAND
                                  , ANALOG_INPUTS
                                  , ANALOG_OUTPUTS
                                  , DIGITAL_INPUTS
                                  , DIGITAL_OUTPUTS
                                  , WORD_INPUTS
                                  , WORD_OUTPUTS
                                  , RXPDO 
                                  , RXPDO_SIZE
                                  , TXPDO 
                                  , TXPDO_SIZE
                                  , SDO };

  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, coe_driver::ModuleDescriptor& module )
  {
    module.initNodeConfigurationFromParams( );
    module.initNodeCoeConfigurationFromParams( true ); // force the sdo configuration if in the ros-param server
  }
    
  inline void toXmlRpcValue( const coe_driver::ModuleDescriptor& module, XmlRpc::XmlRpcValue& xml_value  )
  {
    xml_value[ KeysId[ KeysCode::DESCRIPTION          ]  ] = (std::string) module.getDescription();
    xml_value[ KeysId[ KeysCode::MODEL                ]  ] = (std::string) module.getIdentifier();
    xml_value[ KeysId[ KeysCode::ENABLE_DC            ]  ] = (bool)        module.isDcEnabled();
    xml_value[ KeysId[ KeysCode::SUPPORT_SDO_CA       ]  ] = (bool)        module.isSdoCaSupported();
    xml_value[ KeysId[ KeysCode::LOOP_RATE_DECIMATION ]  ] = (int)         module.getLoopRateDecimation();
    xml_value[ KeysId[ KeysCode::WATCHDOG_DECIMATION  ]  ] = (int)         module.getWatchdogDecimation();
    
    XmlRpcAxisData::toXmlRpcValue   ( module.getAxisCommand()   , xml_value[ KeysId[ KeysCode::AXIS_COMMAND] ] );
    XmlRpcAxisData::toXmlRpcValue   ( module.getAxisFeedback()  , xml_value[ KeysId[ KeysCode::AXIS_FEEDBACK] ] );
    XmlRpcAnalogData::toXmlRpcValue ( module.getAnalogInputs()  , xml_value[ KeysId[ KeysCode::ANALOG_INPUTS] ] );
    XmlRpcAnalogData::toXmlRpcValue ( module.getAnalogOutputs() , xml_value[ KeysId[ KeysCode::ANALOG_OUTPUTS] ] );
    XmlRpcDigitalData::toXmlRpcValue( module.getDigitalInputs() , xml_value[ KeysId[ KeysCode::DIGITAL_INPUTS] ] );
    XmlRpcDigitalData::toXmlRpcValue( module.getDigitalOutputs(), xml_value[ KeysId[ KeysCode::DIGITAL_OUTPUTS] ] );
    XmlRpcWordData::toXmlRpcValue   ( module.getWordInputs()    , xml_value[ KeysId[ KeysCode::WORD_INPUTS] ] );
    XmlRpcWordData::toXmlRpcValue   ( module.getWordOutputs()   , xml_value[ KeysId[ KeysCode::WORD_OUTPUTS] ] );
    
    coe_core::XmlRpcPdo::toXmlRpcValue( module.getTxPdo() ,  xml_value[ KeysId[ KeysCode::TXPDO] ] );
    coe_core::XmlRpcPdo::toXmlRpcValue( module.getRxPdo() ,  xml_value[ KeysId[ KeysCode::RXPDO] ] );
    
    itia::rutils::toXmlRpcValue( (int)module.sizeInputs()  ,  xml_value[ KeysId[ KeysCode::TXPDO_SIZE] ] );
    itia::rutils::toXmlRpcValue( (int)module.sizeOutputs() ,  xml_value[ KeysId[ KeysCode::RXPDO_SIZE] ] );
    if( module.getConfigurationSdo().nEntries() > 0 )
      coe_core::XmlRpcSdo::toXmlRpcValue( module.getConfigurationSdo(),  xml_value[ KeysId[ KeysCode::SDO] ] );
  }

};


}

#endif
