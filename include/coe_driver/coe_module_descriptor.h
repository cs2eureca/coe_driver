
#ifndef __coe__ros__module__descriptor__h__
#define __coe__ros__module__descriptor__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <tuple>
#include <ros/ros.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_utilities.h>
#include <soem/ethercat.h>

namespace coe_driver
{

class ModuleDescriptor
{
public:
  
  typedef std::shared_ptr<ModuleDescriptor> Ptr;
  
  struct  AxisDataEntry {
    coe_core::DataObject*  entry;
    double                            scale;
    double                            offset;
    uint8_t                           pdo_subindex; 
  };
  
  typedef AxisDataEntry AnalogDataEntry;
  
  struct DigitalDataEntry
  {
    coe_core::DataObject*  entry;
    uint8_t                           pdo_subindex; 
    uint8_t                           bit; 
  };
  
  
public:
  
  ModuleDescriptor(  )  
  : axis_name_("nil"), rx_pdo_( ECT_SDO_RXPDOASSIGN ), tx_pdo_( ECT_SDO_TXPDOASSIGN )
  {
      // nothing to do so far
  }
  
  ModuleDescriptor( XmlRpc::XmlRpcValue& config )
  : rx_pdo_( ECT_SDO_RXPDOASSIGN ), tx_pdo_( ECT_SDO_TXPDOASSIGN )
  {
    if( !initFromParam( config ) )
    {
      ROS_FATAL("Error in extracting data from ROS PARAM SERVER. Abort");
      throw std::runtime_error("Error in extracting data from ROS PARAM SERVER. Abort");
    }
  }
  ModuleDescriptor( const int iSlave, bool support_dc, bool support_sdoca )
  : rx_pdo_( ECT_SDO_RXPDOASSIGN )
  , tx_pdo_( ECT_SDO_TXPDOASSIGN )
  {
    if( !initFromSoem( iSlave ) )
    {
      ROS_FATAL("Error in extracting data from SOEM. Abort");
      throw std::runtime_error("Error in extracting data from SOEM. Abort");
    } 
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool initFromParam ( const XmlRpc::XmlRpcValue& config );
  bool initFromSoem  ( const uint16_t iSlave );
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  void updateInputs ( const uint8_t* inputs , bool prepended_time );
  void updateOutputs( const uint8_t* outputs, bool prepended_time );
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const int&            getAddress          ( ) const { return address_;        }
  const std::string&    getDescription      ( ) const { return description_;    }
  const std::string&    getIdentifier       ( ) const { return identifier_;     }
  const bool            isDcSupported       ( ) const { return support_dc_;     }
  const bool            isSdoCaSupported    ( ) const { return support_sdoca_;  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const std::string&                             getAxisName       ( ) const { return axis_name_;      }
  const std::map<std::string,AxisDataEntry>&     getAxisCommand    ( ) const { return axis_command_;   }
  const std::map<std::string,AxisDataEntry>&     getAxisFeedback   ( ) const { return axis_feedback_;  }
  const std::map<std::string,AnalogDataEntry>&   getAnalogInputs   ( ) const { return analog_inputs_;  }
  const std::map<std::string,AnalogDataEntry>&   getAnalogOutputs  ( ) const { return analog_outputs_; }
  const std::map<std::string,DigitalDataEntry>&  getDigitalInputs  ( ) const { return digital_inputs_; }
  const std::map<std::string,DigitalDataEntry>&  getDigitalOutputs ( ) const { return digital_inputs_; }
  
  const AxisDataEntry&      getAxisCommand      ( const std::string& k ) const { return axis_command_    .at(k);  }
  const AxisDataEntry&      getAxisFeedback     ( const std::string& k ) const { return axis_feedback_   .at(k);  }
  const AnalogDataEntry&    getAnalogInputs     ( const std::string& k ) const { return analog_inputs_   .at(k);  }
  const AnalogDataEntry&    getAnalogOutputs    ( const std::string& k ) const { return analog_outputs_  .at(k);  }
  const DigitalDataEntry&   getDigitalInputs    ( const std::string& k ) const { return digital_inputs_  .at(k);  }
  const DigitalDataEntry&   getDigitalOutputs   ( const std::string& k ) const { return digital_outputs_ .at(k);  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const std::string         getSoemInputId      ( ) const { return identifier_ + "_txpdo"; }
  const std::string         getSoemOutputId     ( ) const { return identifier_ + "_rxpdo"; }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const coe_core::Pdo&      getRxPdo            ( ) const { return rx_pdo_; }
  const coe_core::Pdo&      getTxPdo            ( ) const { return tx_pdo_; }
  const coe_core::Sdo&      getConfigurationSdo ( ) const { return configuration_sdo_; }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  coe_core::Pdo&            getRxPdo            ( )       { return rx_pdo_; }
  coe_core::Pdo&            getTxPdo            ( )       { return tx_pdo_; }
  
  //utilities
  const std::string         to_string( const std::string what = "input" );
  void                      to_param( XmlRpc::XmlRpcValue& config ) const;
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
private:
  
  int                           address_;
  std::string                   description_;
  std::string                   identifier_;
  bool                          default_;
  bool                          support_dc_;
  bool                          support_sdoca_;
  std::string                   axis_name_;
  coe_core::Sdo                 configuration_sdo_;
  coe_core::Pdo                 rx_pdo_;
  coe_core::Pdo                 tx_pdo_;
  
  std::map< std::string, AxisDataEntry  >    axis_feedback_;
  std::map< std::string, AxisDataEntry  >    axis_command_;
  
  std::map< std::string, AnalogDataEntry >   analog_inputs_;
  std::map< std::string, AnalogDataEntry >   analog_outputs_;
  
  std::map< std::string, DigitalDataEntry >  digital_inputs_;
  std::map< std::string, DigitalDataEntry >  digital_outputs_;
  
};

typedef ModuleDescriptor::Ptr  ModuleDescriptorPtr;  


}

#endif
